var applicationConfiguration = require('./config/config');

exports.config = {
	//seleniumAddress: 'http://localhost:4444/wd/hub',
	specs: applicationConfiguration.assets.testsE2e,
	suites: {
		auth: ['app/tests-e2e/authentication.view.test.js','app/tests-e2e/change-password.client.view.test.js'],
		users: 'app/tests-e2e/users-crud.client.test.js',
		temples: 'app/tests-e2e/temple-crud.client.test.js',
		people:'app/tests-e2e/people-crud.client.test.js'
	},
	onPrepare: function() {
  		browser.driver.manage().window().maximize();
	}
	// onPrepare: function() {
 //        var e2e = require('./public/lib/protractor/e2e.js');
 //    }

}

