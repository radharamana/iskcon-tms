'use strict';

angular.module('core').factory('Select2', function(){
	function Select2(sub, field){
		this.sub = sub;
		this.field = field;
		
	}

	Select2.setDOs = function(selects, object){
		for(var select in selects){
			selects[select].setDO(object);
		}
	};

	Select2.setValues = function(object, selects){
		for(var select in selects){
			//selects[select][Object.keys(selects[select])[0]].setValue(object);
			selects[select].setValue(object);
		}
	};

	return ( Select2 );
});



