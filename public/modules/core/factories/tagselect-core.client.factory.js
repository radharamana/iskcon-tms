'use strict';

angular.module('core').factory('TagSelect', ['Select2', function(Select2){
	function TagSelect(baseObj, sub, field, tags, $scope){
		Select2.call(this, sub, field);
		this.tags = tags;
		this.scope = $scope;

		this.opts = {
				initSelection: function (element, callback) {
					callback(element);
				},
				multiple: true,
				tags: function() {
					return tags;
				},
				width: '100%'
			};
		
		this.values = [];
		this.new = '';
		this.baseObj = baseObj;
		this.unwatch = false;

		this.scope.$watch('selects.'+this.field+'.values',this.selectWatch(this, this.baseObj)); 
		
		//this.scope.unwatchBaseObj = this.scope.$watch(baseObj, this.baseObjWatch(this));
		
		
	}

	TagSelect.prototype = Object.create(Select2.prototype);

	TagSelect.prototype.constructor = TagSelect;

	
	
	TagSelect.prototype.selectWatch = function(self, objectName){
		return function(){
			self.setDO(objectName);
			//console.log('selectWatch self.field:'+self.field);
		};
	};

	TagSelect.prototype.baseObjWatch = function(self){
		return function(){
			self.setValue(self.scope[self.baseObj]);
			//console.log('watch baseObj self.field:'+self.field);
		};
	};	

	TagSelect.prototype.setDO = function(baseObj){
		var i;
		
		if(!this.scope[baseObj]){
			this.scope[baseObj] = {};
		}
		if(this.sub){
			if(!this.scope[baseObj][this.sub]){
				this.scope[baseObj][this.sub] = {};
			}
			this.scope[baseObj][this.sub][this.field] = [];
			for(i = 0; i < this.values.length; i++){
				this.scope[baseObj][this.sub][this.field].push(this.values[i].text);
			}
		}else{
			this.scope[baseObj][this.field] = [];
			for(i = 0; i < this.values.length; i++){
				this.scope[baseObj][this.field].push(this.values[i].text);
			}

		}

	};

	TagSelect.prototype.setValue = function(object){
		var i;
		
		if(this.sub){
			//console.log("setting select for "+this.field);
			if(object[this.sub] && object[this.sub][this.field]){
				this.values = [];
				//console.log("setting select for "+this.field+" with length:"+object[this.sub][this.field].length+" values.length:"+this.values.length);
		
				for(i = 0; i < object[this.sub][this.field].length; i++){
					this.values.push({id: object[this.sub][this.field][i], text: object[this.sub][this.field][i]});
				}	
			}
		}else{
			if(object[this.field]){
				this.values = [];
				//console.log("setting select for "+this.field+" with length:"+object[this.field].length);
		
				for(i = 0; i < object[this.field].length; i++){
					this.values.push({id: object[this.field][i], text: object[this.field][i]});
				}	
			}

		}

		
	};

	TagSelect.prototype.addTag = function($event){
		$event.preventDefault();
    	$event.stopPropagation();
		if(this.new){
			this.tags.push(this.new);
			this.values.push({id: this.new, text:this.new});
			this.new = '';
		}
	};

	return (TagSelect);
}]);