'use strict';

angular.module('core').factory('LookupSelect', ['Select2',function(Select2){
	

	function LookupSelect(baseObj, sub, field, placeHolder, queryFunc, $scope){
		Select2.call(this, sub, field);
		
		this.value = '';
		this.scope = $scope;

		this.opts = {
		 	placeholder: placeHolder,
		 	allowClear: true,
		 	minimumInputLength: 2,
		};

		if(queryFunc){
			this.opts.query = queryFunc;
		}

		$scope.$watch('selects.'+field+'.value',this.watchCallback(this, baseObj)); 
	}

	LookupSelect.prototype = Object.create(Select2.prototype);

	LookupSelect.prototype.constructor = LookupSelect;

	LookupSelect.prototype.setDO = function(objectName){
		if(!this.value)return;
		
		var watchers = this.scope.$$watchers;
		this.scope.$$watchers = [];

		if(!this.scope[objectName]){
			this.scope[objectName] = {};
		}
		if(this.sub){
			if(!this.scope[objectName][this.sub]){
				this.scope[objectName][this.sub] = {};
			}
			this.scope[objectName][this.sub][this.field] = this.value.id;
		}else{
			this.scope[objectName][this.field] = this.value.id;
		}

		this.scope.$$watchers = watchers;
	};	

	LookupSelect.prototype.watchCallback = function(self, objectName){
		return function(){
			self.setDO(objectName);
		};
	};

	return (LookupSelect);
}]);