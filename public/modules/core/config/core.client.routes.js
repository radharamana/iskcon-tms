'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider', 
	function($stateProvider, $urlRouterProvider) {
		// Redirect to home view when route not found
		$urlRouterProvider.otherwise('/');

		// Home state routing
		$stateProvider.
		state('home', {
			url: '/',
			templateUrl: 'modules/core/views/home.client.view.html'
		}).
		state('temple', {
			url: '/t/:temple',
			abstract:true,
			templateUrl: 'modules/core/views/parent.temple.client.view.html'

		}).
		state('temple.home',  {
			url: '/home',
			templateUrl: 'modules/core/views/home.temple.client.view.html'
			
		})
		;
	}
]);