'use strict';


/*
function setSel2Fields($scope, object){
	if(!$scope.sel2Fields){
		console.log('No $scope.sel2Fields');
		return;
	}

	for(var i=0; i < $scope.sel2Fields.length; i++){
		var selField = $scope.sel2Fields[i];
		if(!person[selField.sub]){
			person[selField.sub] = {};
		}
		if(selField.isTag){
			person[selField.personType][selField.personField] = [];
			for(var i2 = 0; i2 < $scope.selects[selField.local].length; i2++){
				var selItem = $scope.selects[selField.local][i2];
				
				person[selField.personType][selField.personField].push(selItem.id);

			}	
		}else if($scope.selects[selField.local]){
			person[selField.personType][selField.personField] = $scope.selects[selField.local].id;			
		}
	}
}

function getSel2FieldsFromPerson(person, $scope){
	for(var i=0; i < $scope.sel2Fields.length; i++){
		var selField = $scope.sel2Fields[i];
		if($scope.person[selField.personType] && $scope.person[selField.personType][selField.personField]){
			var personValue = $scope.person[selField.personType][selField.personField];
			if(selField.isTag){
				$scope.selects[selField.local] = [];
				for(var i2 = 0; i2 < personValue.length; i2++){
					$scope.selects[selField.local].push({id: personValue[i2], text: personValue[i2]});
				}	
			}else {
				$scope.selects[selField.local] = {id: personValue._id, text: personValue.name.firstName + ' ' + personValue.name.lastName};			
			}	
		
		}
		
	}	
}
*/

angular.module('core').controller('HeaderController', ['$scope', '$stateParams', '$rootScope', 'Authentication', 'Menus', 'Temples',
	function($scope, $stateParams, $rootScope ,Authentication, Menus, Temples) {
		$scope.authentication = Authentication;
		$scope.isCollapsed = false;
		$scope.menu = Menus.getMenu('topbar');
		$rootScope.home = '/#!/';
		
		
		$scope.toggleCollapsibleMenu = function() {
			$scope.isCollapsed = !$scope.isCollapsed;
		};

		// Collapsing the menu after navigation
		$scope.$on('$stateChangeSuccess', function() {
			$scope.isCollapsed = false;
		});

		// Find existing Temple
		// $scope.findTemple = function() {
		// 	if(!$rootScope.temple){
		// 		if(!$stateParams.temple){
		// 			return;
		// 		}
		// 		$rootScope.temple = Temples.get({ 
		// 		temple: $stateParams.temple
		// 		},function(temple, res){
	 // 				$rootScope.home += 't/'+temple.pathname+'/';	
	 // 			});	
		// 	}
		// };
		
	}
]);

// function(temple, res){
// 				$scope.home += 't/'+temple.pathname;	
// 			}