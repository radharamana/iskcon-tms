'use strict';


angular.module('core').controller('HomeController', ['$scope', '$rootScope','$stateParams', '$location', '$timeout', 'Authentication','Temples', 'Articles',
	function($scope, $rootScope, $stateParams, $location, $timeout, Authentication, Temples, Articles) {
		// This provides Authentication context.
		$scope.authentication = Authentication;

		$scope.findTemples = function() {
			if(!$scope.authentication.user){
				$scope.temples = Temples.query();
				$scope.temples.$promise.then(function(res){
					//console.log(res);
				});

				$rootScope.home = '/#!/';
				delete $rootScope.temple;	
			}else if($scope.authentication.user.temple){
				$timeout(function(){
				 	$location.path('/t/'+$scope.authentication.user.temple.pathname+'/home');
				});
			}else{
				$location.path($rootScope.home.substr(3));
			}
		};

		// Find existing Temple
		$scope.findTemple = function() {
			if(!$rootScope.temple){
					if($scope.authentication.user){
						$rootScope.temple = $scope.authentication.user.temple;
						$rootScope.home = '/#!/t/'+$rootScope.temple.pathname+'/';	
	 		 			$rootScope.stateHome = 't/'+$rootScope.temple.pathname+'/';		
					}else{
						$rootScope.temple = Temples.get({ 
							temple: $stateParams.temple
						},function(temple, res){
	 						$rootScope.home = '/#!/t/'+temple.pathname+'/';	
	 						$rootScope.stateHome = 't/'+temple.pathname+'/';
	 					});			
					}
					
				
			}
		};

		
	}


]);