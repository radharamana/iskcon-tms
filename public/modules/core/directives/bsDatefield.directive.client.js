'use strict';

angular.module('core').directive('bsDatefield',function() {
    return {
      require: 'ngModel',
      link: function (scope, element, attrs, ngModelCtrl) {
        if(attrs.bsDatePattern){
          if(attrs.bsDatePattern.charAt(0) === '/'){
            attrs.bsDatePattern = attrs.bsDatePattern.slice(1,-1);
          }
          var dRegex = new RegExp(attrs.bsDatePattern);
        } 
         

        var dateFormat = attrs.bsDatefield || 'YYYY/MM/DD';
        var isRequired = (attrs.required?true:false);


        ngModelCtrl.$parsers.unshift(function (viewValue) {

          if(Object.prototype.toString.call(viewValue) === '[object Date]'){
              ngModelCtrl.$setValidity('datefield', true);
              return moment(viewValue).format(dateFormat);
          }
          //convert string input into moment data model
          var parsedMoment = moment(viewValue, dateFormat);

          var isValid = (dRegex?parsedMoment.isValid() && dRegex.test(viewValue):parsedMoment.isValid());
          var parsedDate;
          if(!isValid && !isRequired && viewValue === ''){
            isValid = true;
            parsedDate = '';
          }else{
            parsedDate = parsedMoment.format(dateFormat);
          }
          //toggle validity
          ngModelCtrl.$setValidity('datefield', isValid);

          //return model value
          return isValid ? parsedDate: undefined;
        });

        // ngModelCtrl.$formatters.push(function (modelValue) {

        //   var isModelADate = angular.isDate(modelValue) || moment(modelValue).isValid();
        //   ngModelCtrl.$setValidity('datefield', isModelADate);

        //   return isModelADate ? moment(modelValue).format(dateFormat) : undefined;
        // });
      }
    };
  });