'use strict';

angular.module('core').directive('awDatepickerPattern',function() {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function(scope,elem,attrs,ngModelCtrl) {
      if(attrs.awDatepickerPattern.charAt(0) === '/'){
        attrs.awDatepickerPattern = attrs.awDatepickerPattern.slice(1,-1);
      }
      // else{
      //   regexStr = attrs.awDatepickerPattern;
      // }

      var dRegex = new RegExp(attrs.awDatepickerPattern);
      
      ngModelCtrl.$parsers.unshift(function(value) {
        
        if (typeof value === 'string') {
          var isValid = dRegex.test(value);
          ngModelCtrl.$setValidity('datep',isValid);
          if (!isValid) {
            return undefined;
          }
        }
        
        return value;
      });
      
    }
  };
});
