'use strict';

angular.module('core').directive('addressConverter',function() {
  return {
    //restrict: 'A',
    require: 'ngModel',
    link: function(scope,elem,attrs,ngModelCtrl) {
      var numBuild;
      ngModelCtrl.$parsers.push(function(googleAddr) {
          // Do to model conversion
          
          return {
            numberBuilding: numBuild, 
            formatted_address:googleAddr.formatted_address, 
            lng:googleAddr.geometry.location.lng,  
            lat:googleAddr.geometry.location.lat
          };
        });

        ngModelCtrl.$formatters.push(function(dbAddr) {
          if(dbAddr){
            numBuild = dbAddr.numberBuilding;  
          }
          return dbAddr;
        });
      
    }
  };
});
