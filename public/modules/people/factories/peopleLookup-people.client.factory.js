'use strict';

angular.module('core').factory('PeopleLookup', ['LookupSelect', 'People' ,function(LookupSelect, People){
	function PeopleLookup(objectName, sub, field, placeHolder, templePath, initLevel, $scope){
		LookupSelect.call(this, objectName, sub, field, placeHolder, null, $scope);
		this.initLevel = initLevel;
		
		this.opts = {
			placeholder: placeHolder,
			allowClear: true,
			minimumInputLength: 2,
			query: function(query){
					var data = {results: []};
					 People.query({
						temple: templePath,
						initiatedLevel: initLevel
					}, function(people, res){
						for(var i = 0; i<people.length; i++){
								data.results.push({
									id: people[i]._id, 
									text:people[i].displayName
								});	
						}
						query.callback(data);
					});
				}
		};
	}

	PeopleLookup.prototype = Object.create(LookupSelect.prototype);

	PeopleLookup.prototype.constructor = PeopleLookup;

	//takes the selected person value from the root person object and sets the name and id on the control
	PeopleLookup.prototype.setValue = function(person){
		var selPerson; 
		if(this.sub && person[this.sub]){
			selPerson = person[this.sub][this.field];	
		}else{
			selPerson = person[this.field];
		}
		
		if(selPerson){
			this.value = {id: selPerson._id, text: selPerson.displayName};
		}else{
			this.value = null;
		}
		
	};

	return (PeopleLookup);

}]);