'use strict';

(function() {
	// People Controller Spec
	describe('People Controller Tests', function() {
		// Initialize global variables
		var PeopleController,
		scope,
		$httpBackend,
		$stateParams,
		$location,
		Select2
		;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($injector, $controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			//$rootScope = _$rootScope_;
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;
			Select2 = $injector.get('Select2');
			$stateParams.temple = 'templeX';
			console.log('initializing');
			// Initialize the People controller.
			PeopleController = $controller('PeopleController', {
				$scope: scope,
				$stateParams: $stateParams
				//$rootScope: $rootScope,
				

			});
		}));

		
		it('$scope.find() should create an array with at least one Person object fetched from XHR', inject(function(People) {
			// Create sample Person using the People service
			var samplePerson = new People({
				name: {fistName: 'New Person', lastName:'Last' } 
			});

			// Create a sample People array that includes the new Person
			var samplePeople = [samplePerson];

			// Set GET response
			$httpBackend.expectGET('t/templeX/people').respond(samplePeople);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.people).toEqualData(samplePeople);
		}));

		it('PeopleLookup query should test that query function gets array of people', inject(function(People, PeopleLookup) {
			// Create sample Person using the People service
			var samplePeople = [{ id: '123456', displayName:'person a' },
				{id: '123457', displayName:'person b'}];

			// Create a sample People array that includes the new Person
			//var samplePeople = [samplePerson];

			// Set GET response
			$httpBackend.expectGET('t/templeX/people?initiatedLevel=0').respond(samplePeople);

			// Run controller functionality
			var queryCb = {
				data: {},
				callback: function(data){
					queryCb.data = data;
				}
			};
			scope.selects.nextOfKin.opts.query(queryCb); 
			$httpBackend.flush();

			// Test scope value
			expect(queryCb.data.results[0].text).toEqual(samplePeople[0].displayName);
			expect(queryCb.data.results[1].text).toEqual(samplePeople[1].displayName);
		}));


		it('$scope.findOne() should create an array with one Person object fetched from XHR using a personId URL parameter', inject(function(People) {
			// Define a sample Person object
			var samplePerson = new People({
				name: {firstName: 'New Person', lastName:'Last' } 
			});

			// Set the URL parameter
			$stateParams.personId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/t\/templeX\/people\/([0-9a-fA-F]{24})$/).respond(samplePerson);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.person).toEqualData(samplePerson);
		}));

		it('$scope.findOne() should be able to add a new tag', inject(function(People) {
			// Define a sample Person object
			var samplePerson = new People({
				name: {firstName: 'New Person', lastName:'Last' }, 
				displayName: 'New Person Last'
			});

			// Set the URL parameter
			$stateParams.personId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/t\/templeX\/people\/([0-9a-fA-F]{24})$/).respond(samplePerson);

			// Run controller functionality
			scope.findOne();
			var mockEvent = {
				preventDefault:function(){},
				stopPropagation:function(){}
			};
			scope.selects.courses.new = 'New Course';
			scope.selects.courses.addTag(mockEvent);
			$httpBackend.flush();

			// Test scope value
			expect(scope.selects.courses.values[0].text).toBe('New Course');
			expect(scope.person).toEqualData(samplePerson);
		}));
		
		it('$scope.findOne() should populate the selects array properly', inject(function(People) {
			// Define a sample Person object
			var samplePersonGet = new People({
				_id: '525a8422f6d0f87f0e407a33',
				name: {firstName: 'New Person', lastName:'Last' },
				displayName: 'New Person Last',
				sex: 'Male',
				race: 'Indian',
				contact:{
					relationshipManager:{_id:'111111111', displayName:'John relation'}
				},
				devotee: {
					skills: ['Kirtana'],
					participation: ['Diety Seva'],
					courses: ['Bhakti-Sadacara'],
					guru: {_id:'111111111', displayName:'John guru'}
				},
				resident:{
					nextOfKin: {_id:'111111111', displayName:'John nextOfKin'},
					medicalConditions: ['Asthma']
				}
				
			});

			var samplePersonScope = new People({
				_id: '525a8422f6d0f87f0e407a33',
				name: {firstName: 'New Person', lastName:'Last' }, 
				displayName: 'New Person Last',
				sex: 'Male',
				race: 'Indian',
				contact:{
					relationshipManager:'111111111'
				},
				devotee: {
					skills: ['Kirtana'],
					participation: ['Diety Seva'],
					courses: ['Bhakti-Sadacara'],
					guru: '111111111'
				},
				resident:{
					nextOfKin: '111111111',
					medicalConditions: ['Asthma']
				}
				
			});

			// Set the URL parameter
			$stateParams.personId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/t\/templeX\/people\/([0-9a-fA-F]{24})$/).respond(samplePersonGet);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.selects.skills.values[0].text).toBe('Kirtana');
			expect(scope.selects.medicalConditions.values[0].text).toBe('Asthma');
			expect(scope.selects.courses.values[0].text).toBe('Bhakti-Sadacara');
			expect(scope.selects.participation.values[0].text).toBe('Diety Seva');
			//console.log('scope.selects.nextOfKin:'+scope.selects.nextOfKin.value);
			expect(scope.selects.nextOfKin.value.text).toBe('John nextOfKin');
			expect(scope.selects.guru.value.text).toBe('John guru');
			expect(scope.selects.relationshipManager.value.text).toBe('John relation');

			expect(scope.person).toEqualData(samplePersonScope);
		}));


		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(People) {
			// Create a sample Person object
			var samplePersonPostData = new People({
				name: {firstName: 'New Person', lastName:'Last' }, 
				sex: 'Male',
				race: 'Indian'

			});

			// Create a sample Person response
			var samplePersonResponse = new People({
				_id: '525cf20451979dea2c000001',
				name: {firstName: 'New Person', lastName:'Last' }, 
				sex: 'Male',
				race: 'Indian'
			});

			// Fixture mock form input values
			scope.person.name = {firstName: 'New Person', lastName:'Last' };
			

			// Set POST response
			$httpBackend.expectPOST('t/templeX/people', samplePersonPostData).respond(samplePersonResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			
			// Test URL redirection after the Person was created
			expect($location.path()).toBe('/t/templeX/people');
		}));

		it('$scope.create() with new person be able to set values on all the selects', inject(function(People) {
			// Create a sample Person object
			var samplePersonPostData = new People({
				name: {firstName: 'New Person', lastName:'Last' }, 
				sex: 'Male',
				race: 'Indian',
				devotee: {
					skills: ['Kirtana'],
					participation: ['Diety Seva'],
					courses: ['Bhakti-Sadacara'],
					guru: '111111111'
				},
				resident:{
					nextOfKin: '111111111',
					medicalConditions: ['Asthma']
				},
				contact:{
					relationshipManager:'111111111'
				}
			});

			// Create a sample Person response
			var samplePersonResponse = new People({
				_id: '525cf20451979dea2c000001',
				name: {firstName: 'New Person', lastName:'Last' }, 
				sex: 'Male',
				race: 'Indian',
				contact:{
					relationshipManager:'111111111'
				},
				devotee: {
					skills: ['Kirtana'],
					participation: ['Diety Seva'],
					courses: ['Bhakti-Sadacara'],
					guru: '111111111'
				},
				resident:{
					nextOfKin: '111111111',
					medicalConditions: ['Asthma']
				}
				
			});

			// Fixture mock form input values
			//console.log('setting values');
			scope.person.name = {firstName: 'New Person', lastName:'Last' };
			scope.selects.skills.values = [{id:'01', text:'Kirtana'}];
			scope.selects.medicalConditions.values = [{id:'01', text:'Asthma'}];
			scope.selects.participation.values = [{id:'01', text:'Diety Seva'}];
			scope.selects.courses.values = [{id:'01', text:'Bhakti-Sadacara'}];
			scope.selects.relationshipManager.value = {id:'111111111', text:'personx'};
			scope.selects.guru.value = {id:'111111111', text:'personx'};
			scope.selects.nextOfKin.value = {id:'111111111', text:'personx'};
			//console.log("applying");
			scope.$apply();
			
			// Set POST response
			$httpBackend.expectPOST('t/templeX/people', samplePersonPostData).respond(samplePersonResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			
			// Test URL redirection after the Person was created
			expect($location.path()).toBe('/t/templeX/people');
		}));

		it('$scope.update() should update a valid Person', inject(function(People) {
			// Define a sample Person put data
			var samplePersonPutData = new People({
				_id: '525cf20451979dea2c000001',
				name: {firstName: 'New Person', lastName:'Last' }, 
				sex: 'Male',
				race: 'Indian'
			});


			// Mock Person in scope
			scope.person = samplePersonPutData;

			// Set PUT response
			$httpBackend.expectPUT(/t\/templeX\/people\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/t/templeX/people');
		}));

		

		it('$scope.update() check that selects are working', inject(function(People) {
			// Define a sample Person put data
			var samplePersonPutData = new People({
				_id: '525a8422f6d0f87f0e407a33',
				name: {firstName: 'New Person', lastName:'Last' }, 
				sex: 'Male',
				race: 'Indian',
				contact:{
					relationshipManager:{_id:'111111111', name:{firstName:'John', lastName:'relatoion'}}
				},
				devotee: {
					skills: ['Kirtana'],
					participation: ['Diety Seva'],
					courses: ['Bhakti-Sadacara'],
					guru: {_id:'111111111', name:{firstName:'John', lastName:'guru'}}
				},
				resident:{
					nextOfKin: {_id:'111111111', name:{firstName:'John', lastName:'nextOfKin'}},
					medicalConditions: ['Asthma']
				}
				
			});

			var samplePersonPutResponse = new People({
				_id: '525a8422f6d0f87f0e407a33',
				name: {firstName: 'New Person', lastName:'Last' }, 
				sex: 'Male',
				race: 'Indian',
				contact:{
					relationshipManager:'111111111'
				},
				devotee: {
					skills: ['Kirtana'],
					participation: ['Diety Seva'],
					courses: [],
					guru: '111111111'
				},
				resident:{
					nextOfKin: '111111112',
					medicalConditions: ['Asthma','Cold']
				}
				
			});
			
			
			// Mock Person in scope
			//console.log('changing values'); 
			//console.log('medicalConditions.values.length:'+scope.selects.medicalConditions.values.length);
			scope.person = samplePersonPutData;
			//$stateParams.personId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			//$httpBackend.expectGET(/t\/templeX\/people\/([0-9a-fA-F]{24})$/).respond(samplePersonPutData);

			// Run controller functionality
			//scope.findOne();
			//$httpBackend.flush();


			//scope.$apply();
			//console.log('Select2:'+Select2.setValue);
			Select2.setValues(scope.person, scope.selects);
			//console.log('medicalConditions.values.length:'+scope.selects.medicalConditions.values.length);
			scope.selects.medicalConditions.values.push({id:'02', text:'Cold'});
			//console.log('courses.values.length:'+scope.selects.courses.values.length);
			scope.selects.courses.values.splice(0,1);
			//console.log("scope.selects.courses.values:"+scope.selects.courses.values.length);
			scope.selects.nextOfKin.value = {id:'111111112', text:'Bob'};
			//console.log("before changes applyed");
			scope.$apply();
			

			$httpBackend.expectPUT(/t\/templeX\/people\/([0-9a-fA-F]{24})$/,samplePersonPutResponse).respond();

			scope.update();
			$httpBackend.flush();

			expect($location.path()).toBe('/t/templeX/people');
		}));

		it('$scope.remove() should send a DELETE request with a valid personId and remove the Person from the scope', inject(function(People) {
			// Create new Person object
			var samplePerson = new People({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new People array and include the Person
			scope.people = [samplePerson];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/t\/templeX\/people\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(samplePerson);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.people.length).toBe(0);
		}));
	});
}());