'use strict';

//People service used to communicate People REST endpoints
angular.module('people').factory('People', ['$resource',
	function($resource) {
		return $resource('t/:temple/people/:personId', { personId: '@_id', temple: '@temple.pathname'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);