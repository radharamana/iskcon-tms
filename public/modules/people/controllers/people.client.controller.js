'use strict';



var ModalInstanceCtrl = function ($scope, $modalInstance, comment) {
	$scope.comment = {};
	$scope.comment.comment = comment;
	$scope.cancelComment = function(){
		$modalInstance.dismiss('cancel');
	};

	$scope.saveComment = function(){
		$modalInstance.close($scope.comment.comment);
	};
};

// People controller
angular.module('people').controller('PeopleController', [
	'$scope', '$stateParams', '$location', '$window', '$rootScope', '$timeout','$modal','Authentication', 'People','dialogs','$http','uiGridConstants', 
	function($scope, $stateParams, $location, $window, $rootScope, $timeout, $modal, Authentication, People, $dialogs, $http, uiGridConstants ) {
		//defaults
		
		$scope.person = {};
		$scope.comment = {};
		$scope.person.sex ='Male';
		$scope.person.race = 'Indian';

		$scope.authentication = Authentication;



		// Create new Person
		$scope.create = function() {
			 
			// Create new Person object
			var person = new People (this.person);
			if(this.comment.comment){
				if($scope.authentication.user.person){
					this.comment.person = $scope.authentication.user.person._id;
				}
				this.comment.date = new Date();
				person.comments = [];
				person.comments.push(this.comment);
			}

			// Redirect after save
			person.$save({
				temple: $stateParams.temple
			}, function(response) {
				$location.path('t/'+$stateParams.temple+'/people');
			}, function(errorResponse) {
				$scope.errors = errorResponse.data;
				$window.scrollTo(0,0);
			});

			// Clear form fields
			//this.name = '';
		};

		

		// Remove existing Person
		$scope.remove = function( person ) {
			if ( person ) { 
				person.$remove({
					temple: $stateParams.temple
				}, function(res){
					for (var i in $scope.people ) {
						if ($scope.people [i] === person ) {
							$scope.people.splice(i, 1);
						}
					}
				}, function(err){
					$scope.errors = err.data;
				});

			} else {
				$scope.person.$remove(function() {
					$location.path('people');
				});
			}
		};

		
		// Update existing Person
		$scope.update = function() {
			
			var person = $scope.person;
			person.$update({
				temple: $stateParams.temple
			},
			function() {
				$location.path('t/'+$stateParams.temple+'/people');
			}, function(errorResponse) {
				console.error('errors:'+errorResponse.data);
				$scope.errors = errorResponse.data;
				$window.scrollTo(0,0);
			});
		};

		// Find a list of People
		$scope.find = function() {
			if(!$scope.people){
				$scope.people = People.query({
					temple: $stateParams.temple
				});	
			}
		};

		// Find existing Person
		$scope.findOne = function() {
			$scope.person = People.get({ 
				personId: $stateParams.personId,
				temple: $stateParams.temple
			}, function(person, res){
				for(var i=0; person.comments && i<person.comments.length; i++){
					if($scope.authentication.user.person && person.comments[i].person){
						person.comments[i].disabled = ($scope.authentication.user.person._id !== person.comments[i].person._id);		
					}else{
						person.comments[i].disabled = false;
					}
				}
			});
			
		};

		$scope.findDevotees = function(){
			if(!$scope.devotees){
				$scope.devotees = People.query({
					temple: $stateParams.temple,
					initiatedLevel: 2
				});		
			}
		};

		$scope.findGurus = function(){
			if(!$scope.gurus){
				$scope.gurus = People.query({
					temple: $stateParams.temple,
					initiatedLevel: 5
				});		
			}
		};




		$scope.datePickers = {
        	regex: /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$|\A\Z/,
        	birthday:{
        		default: '2000-01-01',
        		open: false
        	},
        	lastContact: {
        		open: false,
        		maxDate: new Date()
        	},
        	dateMovedIn: {
        		open: false,
        		maxDate: new Date()	
        	},
        	comment: {
        		open: false,
        		maxDate: new Date()		
        	}
      	};

		$scope.openDate = function($event, field){
			$event.preventDefault();
    		$event.stopPropagation();
    		if($scope.datePickers[field].default){
    			this.person[field] = $scope.datePickers[field].default;	
    		}
    		$scope.datePickers[field].open = true;
    	};

		$scope.refreshAddresses = function(address) {
		    var params = {address: address, sensor: false, components:'country:ZA'  };
		    return $http.get(
		      'http://maps.googleapis.com/maps/api/geocode/json',
		      {params: params}
		    ).then(function(response) {
		      $scope.addresses = response.data.results;
		    });
		  };

		
		$scope.selects = {
			// skills: new TagSelect('person','devotee', 'skills', ['Kirtana','Class','Cooking','IT'], $scope),
			// medicalConditions: new TagSelect('person','resident', 'medicalConditions', ['Asthma','Bee Sting Allergy','HIV','Gluten Allergy','Heart Disease'], $scope),
			// participation:new TagSelect('person','devotee','participation', ['Attends service', 'Diety Seva', 'Misc Seva', 'Donor', 'Outreach','Management'], $scope),
			//courses: new TagSelect('person','devotee','courses',['Bhakti-Sadacara','Bhakti-Sastri'], $scope),
			//address:{formatted_address:'Libra Road, Germiston, 1401, South Africa'},
			skills:['Kirtana','Class','Cooking','IT'],
			medicalConditions:['Asthma','Bee Sting Allergy','Diabetes','HIV','Gluten Allergy','Heart Disease'],
			participation:['Attends service', 'Diety Seva', 'Misc Seva', 'Donor', 'Outreach','Management'],
			courses:['Bhakti-Sadacara','Bhakti-Sastri'],
			// relationshipManager: new PeopleLookup('person','contact', 'relationshipManager', 'Choose a person', $stateParams.temple, 2, $scope),
			// guru: new PeopleLookup('person','devotee', 'guru', 'Bhakti Swami',  $stateParams.temple, 5, $scope),
			// nextOfKin: new PeopleLookup('person','resident', 'nextOfKin', 'John Relative', $stateParams.temple, 0, $scope)
			};


		$scope.grid = {};
		
		$scope.grid.comment = {
			data: 'person.comments',
			enableColumnResizing : true,
			enableRowSelection: true,
			multiSelect:false,
			enableHorizontalScrollbar: uiGridConstants.scrollbars.NEVER,
			rowHeight: 45,
        	columnDefs: [{
					field: 'date', 
					displayName: 'Date', 
					cellTemplate: '<div class="ngCellText">{{row.entity.date | saDate}}</div>',
					visible: !$scope.authentication.isMobile()
					//,width:'25%'
				},{
					field:'comment', 
					displayName:'Comment',
					width:'50%'
				},{
					field:'person.displayName',
					displayName: 'Commentor',
					visible: !$scope.authentication.isMobile()
				},{
					name: 'actions',
					displayName: '',
					cellTemplate: '<div class="ngCellText"  data-ng-model="row" )">' +
						'<button ng-disabled="row.entity.disabled" ng-click="getExternalScopes().editRowComment(row,$event)">'+
						'<i class="glyphicon glyphicon-edit"/></button></div>',
					width:'10%'
				}

				]
		};

		
		

		$scope.openComment = function($event){
			$event.preventDefault();
    		$event.stopPropagation();
			var modalInstance = $modal.open({
				templateUrl: 'modules/people/views/crud-comment.client.view.html',
				controller: function($scope, $modalInstance){
					$scope.comment = {};
					$scope.cancelComment = function(){
						$modalInstance.dismiss('cancel');
					};

					$scope.saveComment = function(){
						$modalInstance.close($scope.comment.comment);
					};
				}
	      	});

	      	modalInstance.result.then(function(comment){
	      		if(!$scope.person.comments){
	      			$scope.person.comments = [];
	      		}
	      		$scope.person.comments.push({
	      			date: new Date(),
	      			person: $scope.authentication.user.person,
	      			comment: comment
	      		});
	      	});
      	};

      	$scope.peopleGrdScp = {
      		// Remove existing Person
			removeConfirm: function( person ){
				var dlg = $dialogs.confirm('Please confirm','Would you like to delete '+ ( person ? person.displayName : $scope.person.displayName ) + '?');
				dlg.result.then(function(btn){
					$scope.remove(person);
				});
			},
			authentication: Authentication,
			home: $rootScope.home
      	};

      	$scope.commentGrdScp = {
      		editRowComment:function(row, $event){
				$event.preventDefault();
	    		$event.stopPropagation();
	    		$scope.comRow=row;
	    		var modalInstance = $modal.open({
					templateUrl: 'modules/people/views/crud-comment.client.view.html',
					controller: ModalInstanceCtrl,
					resolve: {
	       				 comment: function () {
	          					return $scope.comRow.entity.comment;
	        			}
	      			}
					
		      	});

		      	modalInstance.result.then(function(comment){
		      		$scope.comRow.entity.comment = comment;
		      	 	//$scope.person.comments[$scope.comRow.rowIndex].comment = comment;
		      	 		
		     	});
	    		
			}
      	};

      	$scope.grid.list = {
			data: 'people',
			enableColumnResize : true,
			enableRowSelection: false,
			rowHeight: 45,
			showFooter:false,
			enableFiltering: true,
			showColumnMenu: false,
			filterOptions: { filterText:''},
        	columnDefs: [{
					field: 'displayName', 
					displayName: 'Name', 
				},{
					field:'mobileNumber', 
					displayName:'Mobile'
				},{
					field:'homeNumber',
					displayName: 'Home',
					visible: !$scope.authentication.isMobile()
				},{
					field:'workNumber',
					displayName: 'Work', 
					visible: !$scope.authentication.isMobile()
				},{
					field:'email',
					displayName: 'Email', 
				},{
					name: 'action',
					displayName:'Action',
					cellTemplate: 
						'<div class="ngCellText"  data-ng-model="row" )">' +
						'<button ng-disabled="row.entity.disabled" go-click="{{getExternalScopes().home}}people/{{row.entity._id}}/edit">'+
						'<i class="glyphicon glyphicon-edit"/></button>'+
						'<button ng-disabled="row.entity.disabled" '+
						'ng-show="getExternalScopes().authentication.isAuthorized([&quot;systemAdmin&quot;,&quot;templeAdmin&quot;,&quot;peopleAdmin&quot;])" '+
						'ng-click="getExternalScopes().removeConfirm(row.entity)">'+
						'<i class="glyphicon glyphicon-trash"/></button>'+
						'</div>'
					}

				]
		};
      	
      	
}]);

