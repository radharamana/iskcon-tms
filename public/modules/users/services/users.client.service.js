'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('users').factory('Users', ['$resource',
	function($resource) {
		return $resource('users/:userId', { userId:'@_id' }, {
			update: {
				method: 'PUT'
			}
		});
	}
]);

angular.module('users').factory('TempleUsers', ['$resource',
	function($resource) {
		return $resource('/t/:temple/users/:userId', { userId:'@_id', temple:'@temple.pathname' }, {
			update: {
				method: 'PUT'
			}
		});
	}
]);