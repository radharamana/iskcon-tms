'use strict';

(function() {
	// Users controller Spec
	describe('UsersController', function() {
		// Initialize global variables
		var UsersController,
			scope,
			$httpBackend,
			$stateParams,
			$location;

		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Load the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;
			$stateParams.temple = 'templeX';

			// Initialize the Authentication controller
			UsersController = $controller('UsersController', {
				$scope: scope,
				$stateParams: $stateParams

			});
		}));

		it('$scope.find() should create an array with at least one User object fetched from XHR', inject(function(TempleUsers) {
			// Create sample User using the Users service
			var sampleUser = new TempleUsers({
				email: 'test@test.com',
				roles: []
			});

			// Create a sample Users array that includes the new User
			var sampleUsers = [sampleUser];

			// Set GET response
			$httpBackend.expectGET('/t/templeX/users').respond(sampleUsers);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.users).toEqualData(sampleUsers);
		}));

		it('$scope.findOne() should create an array with one User object fetched from XHR using a UserId URL parameter', inject(function(TempleUsers) {
			// Define a sample User object
			var sampleUser = new TempleUsers({
				email: 'test@test.com',
				roles: []
			});

			// Set the URL parameter
			$stateParams.userId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/t\/templeX\/users\/([0-9a-fA-F]{24})$/).respond(sampleUser);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.crudUser).toEqualData(sampleUser);
		}));

		it('$scope.findOne() with person attached', inject(function(TempleUsers) {
			// Define a sample User object
			var sampleUserData = new TempleUsers({
				email: 'test@test.com',
				roles: [],
				person:{_id:'123456', displayName:'John Doe'}
			});

			var sampleUserResponse = new TempleUsers({
				email: 'test@test.com',
				roles: [],
				person:'123456'
			});

			// Set the URL parameter
			$stateParams.userId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/t\/templeX\/users\/([0-9a-fA-F]{24})$/).respond(sampleUserData);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			expect(scope.selects.person.value.text).toBe('John Doe');
			expect(scope.crudUser).toEqualData(sampleUserResponse);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(TempleUsers) {
			// Create a sample User object
			var sampleUserPostData = new TempleUsers({
				email: 'test@test.com',
				roles: []
			});

			// Create a sample User response
			var sampleUserResponse = new TempleUsers({
				_id: '525cf20451979dea2c000001',
				email: 'test@test.com'
			});

			// Fixture mock form input values
			scope.crudUser = sampleUserPostData;

			// Set POST response
			$httpBackend.expectPOST('/t/templeX/users', sampleUserPostData).respond(sampleUserResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			//expect(scope.name).toEqual('');

			// Test URL redirection after the User was created
			expect($location.path()).toBe('/t/templeX/users');
		}));

		it('$scope.create() with a person attached', inject(function(TempleUsers) {
			// Create a sample User object
			var sampleUserPostData = new TempleUsers({
				email: 'test@test.com',
				roles: []
			});

			// Create a sample User response
			var sampleUserResponse = new TempleUsers({
				_id: '525cf20451979dea2c000001',
				email: 'test@test.com',
				person: '123456'
			});

			scope.crudUser = sampleUserPostData;
			scope.selects.person.value = {id:'123456', text:'personx'};
			scope.$apply(); 

			$httpBackend.expectPOST('/t/templeX/users', sampleUserPostData).respond(sampleUserResponse);

			scope.create();
			$httpBackend.flush();

			expect($location.path()).toBe('/t/templeX/users');
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(TempleUsers) {
			// Create a sample User object
			var sampleUserPostData = new TempleUsers({
				email: 'test@test.com',
				roles: []
			});

			// Create a sample User response
			var sampleUserResponse = new TempleUsers({
				_id: '525cf20451979dea2c000001',
				email: 'test@test.com'
			});

			// Fixture mock form input values
			scope.crudUser = sampleUserPostData;

			// Set POST response
			$httpBackend.expectPOST('/t/templeX/users', sampleUserPostData).respond(sampleUserResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			//expect(scope.name).toEqual('');

			// Test URL redirection after the User was created
			expect($location.path()).toBe('/t/templeX/users');
		}));

		it('$scope.update() should update a valid User', inject(function(TempleUsers) {
			// Define a sample User put data
			var sampleUserPutData = new TempleUsers({
				_id: '525cf20451979dea2c000001',
				email: 'test@test.com',
				roles: []
			});

			// Mock User in scope
			scope.crudUser = sampleUserPutData;

			// Set PUT response
			$httpBackend.expectPUT(/t\/templeX\/users\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/t/templeX/users');
		}));

		it('$scope.update() should update with person attached', inject(function(TempleUsers, Select2) {
			// Define a sample User put data
			var sampleUserPutData = new TempleUsers({
				_id: '525cf20451979dea2c000001',
				email: 'test@test.com',
				roles: [],
				person:{_id:'123456',name:{firstName:'Bob', lastName:'Marley'}}
			});

			var sampleUserPutResp = new TempleUsers({
				_id: '525cf20451979dea2c000001',
				email: 'test@test.com',
				roles: [],
				person:'1234566'
			});

			// Mock User in scope
			scope.crudUser = sampleUserPutData;
			Select2.setValues(scope.crudUser, scope.selects);

			scope.selects.person.value = {id:'1234566', text:'personx'};
			scope.$apply();

			// Set PUT response
			$httpBackend.expectPUT(/t\/templeX\/users\/([0-9a-fA-F]{24})$/, sampleUserPutResp).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/t/templeX/users');
		}));

		it('$scope.remove() should send a DELETE request with a valid userId and remove the User from the scope', inject(function(TempleUsers) {
			// Create new User object
			var sampleUser = new TempleUsers({
				_id: '525a8422f6d0f87f0e407a33',
				roles: []
			});

			// Create new Users array and include the User
			scope.users = [sampleUser];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/t\/templeX\/users\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleUser);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.users.length).toBe(0);
		}));

	});
}());