'use strict';

angular.module('users').directive( 'goClick', function ( $location ) {
  return function ( scope, element, attrs ) {
    var path;

    attrs.$observe( 'goClick', function (val) {
      path = val;
    });

    element.bind( 'click', function () {
      scope.$apply( function () {
       if(path.indexOf('/#!') !== -1){
            path = path.substr(3);
        }
        $location.path( path );
        //scope.apply();
      });
    });
  };
});