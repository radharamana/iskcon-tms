'use strict';

function setScopeRoles($scope, user){
	for (var i = 0; i < user.roles.length; i++) {
					switch(user.roles[i]){
						case 'peopleUser':
							$scope.roles.peopleUser = true;
							break;
						case 'peopleAdmin':
							$scope.roles.peopleAdmin = true;
							break;
						case 'templeAdmin':
							$scope.roles.templeAdmin = true;
							break;
						case 'systemAdmin':
							$scope.roles.systemAdmin = true;
							break;
					}
				}
}

function getScopeRoles($scope){
	var rolesArray = [];
			if($scope.roles.peopleUser)rolesArray.push('peopleUser');
			if($scope.roles.peopleAdmin)rolesArray.push('peopleAdmin');
			if($scope.roles.templeAdmin)rolesArray.push('templeAdmin');
			if($scope.roles.systemAdmin)rolesArray.push('systemAdmin');

	return rolesArray;
}


angular.module('users').controller('UsersController', ['$scope', '$http', '$location', '$stateParams', '$rootScope', 'Authentication', 'Users', 'TempleUsers','Temples','PeopleLookup','Select2','dialogs','People',
	function($scope, $http, $location, $stateParams, $rootScope, Authentication, Users, TempleUsers, Temples, PeopleLookup, Select2, $dialogs, People) {
		$scope.authentication = Authentication;
		$scope.roles = {
			peopleUser: false,
			peopleAdmin: false,
			templeAdmin: false,
			systemAdmin: false	
		};
		$scope.crudUser = {};
		$scope.form = {
			temple: $stateParams.temple
		};

		$scope.findDevotees = function(){
			if(!$scope.devotees){
				$scope.devotees = People.query({
					temple: $stateParams.temple,
					initiatedLevel: 1
				});		
			}
			
		};
		

		//$scope.form.temple = $stateParams.temple;
		//$scope.test = $stateParams.temple;

		// Create new Temple
		$scope.create = function() {
			
			var user;
			if($scope.crudUser.password !== $scope.form.password2){
				$scope.errors = ['Passwords don\'t match'];
				return;
			}
	
			// Create new Temple object
			// var user = new Users ({
			// 	email: this.email,
			// 	password: this.password,
			// 	roles: getScopeRoles($scope),
			// 	temple: this.userTemple
			// });

			if($stateParams.temple){
				user = new TempleUsers(this.crudUser);
				
				
			}else{
				user = new Users(this.crudUser);
			}

			user.roles = getScopeRoles($scope);
			// Redirect after save
			user.$save({
				temple: $stateParams.temple
			},function(response) {
				$location.path(($stateParams.temple?'t/'+$stateParams.temple+'/':'')+'users');
			}, function(errorResponse) {
				$scope.errors = errorResponse.data;
			});

		};

		// Remove existing User
		$scope.removeConfirm = function( user) {
			var dlg = $dialogs.confirm('Please confirm','Would you like to delete '+ ( user ? user.email : $scope.user.email ) + '?');
			dlg.result.then(function(btn){
				$scope.remove(user);
			});
		};

		$scope.remove = function( user ) {
				if ( user ) { 
					user.$remove({temple: $stateParams.temple});
					for (var i in $scope.users ) {
						if ($scope.users [i] === user ) {
							$scope.users.splice(i, 1);
						}
					}
				} else {
					$scope.user.$remove({
						temple: $stateParams.temple
					},function() {
					 	$location.path(($stateParams.temple?'t/'+$stateParams.temple+'/':'')+'users');
					});
				}	
		};

		// Find a User
		$scope.find = function() {
			if($stateParams.temple){
				$scope.users = TempleUsers.query({
					temple: $stateParams.temple
				});
			}else{
				$scope.users = Users.query();	
			}
			
		};

		// Find temples
		$scope.findTemples = function() {
			if(!$scope.temples && !$scope.temple){
				$scope.temples = Temples.query();	
			}
		};

		// Update existing User
		$scope.update = function() {
			var user = $scope.crudUser;
			user.roles = getScopeRoles($scope);
			if($scope.crudUser.password !== $scope.form.password2){
				$scope.errors = ['Passwords don\'t match'];
				return;
			}
			if($scope.initPass === user.password){
				delete user.password;
			}
			
			user.$update({
				temple: $stateParams.temple
			},function() {
				$location.path(($stateParams.temple?'t/'+$stateParams.temple+'/':'')+'users');
			}, function(errorResponse) {
				$scope.errors = errorResponse.data;
				$scope.crudUser.password = $scope.initPass;
				$scope.form2.password2 = $scope.initPass;
			});
		};

		// Find existing User
		$scope.findOne = function() {
			var findCallBack = function(user, res){
				setScopeRoles($scope, user);
				$scope.password2 = user.password;
				$scope.initPass = user.password;
				//Select2.setValues(user, $scope.selects);
			};

			if($stateParams.temple){
				$scope.crudUser = TempleUsers.get({
					userId: $stateParams.userId,
					temple: $stateParams.temple
				}, findCallBack);
			}else{
				$scope.crudUser = Users.get({ 
					userId: $stateParams.userId
				}, findCallBack);
			}
		};



		// $scope.selects = {
		// 	person:  new PeopleLookup('crudUser','','person','Choose a person', $stateParams.temple, 1,$scope)
		// };

		//relationshipManager: new PeopleLookup('person','contact', 'relationshipManager', 'Choose a person', $stateParams.temple, 2, $scope),
			
			
	}
]);

//testing