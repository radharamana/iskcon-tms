'use strict';

angular.module('users').controller('AuthenticationController', ['$scope', '$http', '$location', '$stateParams', 'Authentication',
	function($scope, $http, $location, $stateParams, Authentication) {
		$scope.authentication = Authentication;

		//If user is signed in then redirect back home
		if ($scope.authentication.user) $location.path('/');

		$scope.signup = function() {

			$http.post('/auth/signup', $scope.credentials).success(function(response) {
				//If successful we assign the response to the global user model
				$scope.authentication.user = response;

				//And redirect to the index page
				$location.path('/');
			}).error(function(response) {
				$scope.error = response.message;
			});
		};

		$scope.signin = function() {
			var signinUrl = '/auth/signin';
			var indexUrl = '/';
			if($stateParams.temple){
				signinUrl = '/t/'+$stateParams.temple + signinUrl;
				indexUrl = '/t/'+$stateParams.temple+'/home';
			}
			$http.post(signinUrl, $scope.credentials).success(function(response) {
			
				//If successful we assign the response to the global user model
				$scope.authentication.user = response;
				window.user = response;

				//And redirect to the index page
				$location.path(indexUrl);
			}).error(function(response) {
				$scope.error = response.message;
			});
		};
	}
]);