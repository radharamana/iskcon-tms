'use strict';

// Temples controller
angular.module('temples').controller('TemplesController', ['$scope', '$stateParams', '$location', 'Authentication', 'CrudTemples','dialogs',
	function($scope, $stateParams, $location, Authentication, CrudTemples, $dialogs ) {
		$scope.authentication = Authentication;
		$scope.temple = {};

		// Create new Temple
		$scope.create = function() {
			// Create new Temple object
			var temple = new CrudTemples($scope.temple);
			// var temple = new Temples ({
			// 	name: this.name,
			// 	address: {line1: this.line1, line2: this.line2, line3: this.line3}
			// });

			// Redirect after save
			temple.$save(function(response) {
				$location.path('temples');
			}, function(errorResponse) {
				$scope.errors = errorResponse.data;
			});

			// Clear form fields
			//this.name = '';
		};

		// Remove existing Temple
		$scope.removeConfirm = function( temple ){
			var dlg = $dialogs.confirm('Please confirm','Would you like to delete '+ ( temple ? temple.name : $scope.temple.name ) + '?');
			dlg.result.then(function(btn){
				$scope.remove(temple);
			});
			
		};

		$scope.remove = function( temple ) {
				if ( temple ) { 
					temple.$remove(function(res){
						for (var i in $scope.temples ) {
							if ($scope.temples [i] === temple ) {
								$scope.temples.splice(i, 1);
							}
						}	
					}, function(err){
						$scope.errors = err.data;
					});
					
				} else {
					$scope.temple.$remove(function() {
						$location.path('temples');
					});
				}
		};

		// Update existing Temple
		$scope.update = function() {
			var temple = $scope.temple ;

			temple.$update(function() {
				$location.path('temples');
			}, function(errorResponse) {
				$scope.errors = errorResponse.data;
			});
		};

		// Find a list of Temples
		$scope.find = function() {
			$scope.temples = CrudTemples.query();
		};

		// Find existing Temple
		$scope.findOne = function() {
			$scope.temple = CrudTemples.get({ 
				templeId: $stateParams.templeId
			});
		};
	}
]);