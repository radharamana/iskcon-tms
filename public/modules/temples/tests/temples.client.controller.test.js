'use strict';

(function() {
	// Temples Controller Spec
	describe('Temples Controller Tests', function() {
		// Initialize global variables
		var TemplesController,
		scope,
		$httpBackend,
		$stateParams,
		$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Temples controller.
			TemplesController = $controller('TemplesController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one Temple object fetched from XHR', inject(function(CrudTemples) {
			// Create sample Temple using the Temples service
			var sampleTemple = new CrudTemples({
				name: 'New Temple'
			});

			// Create a sample Temples array that includes the new Temple
			var sampleTemples = [sampleTemple];

			// Set GET response
			$httpBackend.expectGET('temples').respond(sampleTemples);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.temples).toEqualData(sampleTemples);
		}));

		it('$scope.findOne() should create an array with one Temple object fetched from XHR using a templeId URL parameter', inject(function(CrudTemples) {
			// Define a sample Temple object
			var sampleTemple = new CrudTemples({
				name: 'New Temple',
				pathname: 'new'
			});

			// Set the URL parameter
			$stateParams.templeId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/temples\/([0-9a-fA-F]{24})$/).respond(sampleTemple);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.temple).toEqualData(sampleTemple);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(CrudTemples) {
			// Create a sample Temple object
			var sampleTemplePostData = new CrudTemples({
				name: 'New Temple'
			});

			// Create a sample Temple response
			var sampleTempleResponse = new CrudTemples({
				_id: '525cf20451979dea2c000001',
				name: 'New Temple'
			});

			// Fixture mock form input values
			scope.temple = sampleTemplePostData;

			// Set POST response
			$httpBackend.expectPOST('temples', sampleTemplePostData).respond(sampleTempleResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			//expect(scope.name).toEqual('');

			// Test URL redirection after the Temple was created
			expect($location.path()).toBe('/temples');
		}));

		it('$scope.update() should update a valid Temple', inject(function(CrudTemples) {
			// Define a sample Temple put data
			var sampleTemplePutData = new CrudTemples({
				_id: '525cf20451979dea2c000001',
				name: 'New Temple'
			});

			// Mock Temple in scope
			scope.temple = sampleTemplePutData;

			// Set PUT response
			$httpBackend.expectPUT(/temples\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/temples');
		}));

		it('$scope.remove() should send a DELETE request with a valid templeId and remove the Temple from the scope', inject(function(CrudTemples) {
			// Create new Temple object
			var sampleTemple = new CrudTemples({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new Temples array and include the Temple
			scope.temples = [sampleTemple];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/temples\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleTemple);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.temples.length).toBe(0);

			//expect($location.path()).toBe('/temples');
		}));
	});
}());