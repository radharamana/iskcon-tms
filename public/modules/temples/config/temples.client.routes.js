'use strict';

//Setting up route
angular.module('temples').config(['$stateProvider',
	function($stateProvider) {
		// Temples state routing
		$stateProvider.
		state('listTemples', {
			url: '/temples',
			templateUrl: 'modules/temples/views/list-temples.client.view.html'
		}).
		state('createTemple', {
			url: '/temples/create',
			templateUrl: 'modules/temples/views/create-temple.client.view.html'
		}).
		// state('viewTemple', {
		// 	url: '/temples/:templeId',
		// 	templateUrl: 'modules/temples/views/view-temple.client.view.html'
		// }).
		state('editTemple', {
			url: '/temples/:templeId',
			templateUrl: 'modules/temples/views/edit-temple.client.view.html'
		});
	}
]);