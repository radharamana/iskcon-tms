'use strict';

// Configuring the Articles module
angular.module('temples').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Temples', 'temples', 'dropdown', '/temples(/create)?', false, ['systemAdmin']);
		Menus.addSubMenuItem('topbar', 'temples', 'List Temples', 'temples');
		Menus.addSubMenuItem('topbar', 'temples', 'New Temple', 'temples/create');
	}
]);