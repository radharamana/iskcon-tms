'use strict';

//Temples service used to communicate Temples REST endpoints
angular.module('temples').factory('Temples', ['$resource',
	function($resource) {
		return $resource('t/:temple', { 
			temple:'@pathname' 
		});
	}
]);

angular.module('temples').factory('CrudTemples', ['$resource',
	function($resource) {
		return $resource('temples/:templeId',{
			templeId: '@_id'
		} , {
			update: {
				method: 'PUT'
			}
		});
	}
]);