'use strict';
// Init the application configuration module for AngularJS application
var ApplicationConfiguration = function () {
    // Init module configuration options
    var applicationModuleName = 'mean';
    var applicationModuleVendorDependencies = [
        'ngResource',
        'ngCookies',
        'ngAnimate',
        'ngTouch',
        'ngSanitize',
        'ui.router',
        'ui.bootstrap',
        'ui.utils',
        'ui.grid',
        'ui.grid.edit',
        'pascalprecht.translate',
        'dialogs.main',
        'dialogs.default-translations',
        'ui.select',
        'ui.grid.edit',
        'ui.grid.resizeColumns',
        'ui.grid.pinning',
        'ui.grid.selection',
        'ui.grid.moveColumns'
      ];
    // Add a new vertical module
    var registerModule = function (moduleName) {
      // Create angular module
      angular.module(moduleName, []);
      // Add the module to the AngularJS configuration file
      angular.module(applicationModuleName).requires.push(moduleName);
    };
    return {
      applicationModuleName: applicationModuleName,
      applicationModuleVendorDependencies: applicationModuleVendorDependencies,
      registerModule: registerModule
    };
  }();'use strict';
//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);
// Setting HTML5 Location Mode
angular.module(ApplicationConfiguration.applicationModuleName).config([
  '$locationProvider',
  function ($locationProvider) {
    $locationProvider.hashPrefix('!');
  }
]);
//Then define the init function for starting up the application
angular.element(document).ready(function () {
  //Fixing facebook bug with redirect
  if (window.location.hash === '#_=_')
    window.location.hash = '#!';
  //Then init the app
  angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});'use strict';
// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('articles');'use strict';
// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('core');'use strict';
// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('people');'use strict';
// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('temples');'use strict';
// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('users');'use strict';  // Configuring the Articles module
               // angular.module('articles').run(['Menus',
               // 	function(Menus) {
               // 		// Set top bar menu items
               // 		Menus.addMenuItem('topbar', 'Articles', 'articles', 'dropdown', '/articles(/create)?');
               // 		Menus.addSubMenuItem('topbar', 'articles', 'List Articles', 'articles');
               // 		Menus.addSubMenuItem('topbar', 'articles', 'New Article', 'articles/create');
               // 	}
               // ]);
'use strict';
// Setting up route
angular.module('articles').config([
  '$stateProvider',
  function ($stateProvider) {
    // Articles state routing
    $stateProvider.state('listArticles', {
      url: '/articles',
      templateUrl: 'modules/articles/views/list-articles.client.view.html'
    }).state('createArticle', {
      url: '/articles/create',
      templateUrl: 'modules/articles/views/create-article.client.view.html'
    }).state('viewArticle', {
      url: '/articles/:articleId',
      templateUrl: 'modules/articles/views/view-article.client.view.html'
    }).state('editArticle', {
      url: '/articles/:articleId/edit',
      templateUrl: 'modules/articles/views/edit-article.client.view.html'
    });
  }
]);'use strict';
angular.module('articles').controller('ArticlesController', [
  '$scope',
  '$stateParams',
  '$location',
  'Authentication',
  'Articles',
  function ($scope, $stateParams, $location, Authentication, Articles) {
    $scope.authentication = Authentication;
    $scope.create = function () {
      var article = new Articles({
          title: this.title,
          content: this.content
        });
      article.$save(function (response) {
        $location.path('articles/' + response._id);
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
      this.title = '';
      this.content = '';
    };
    $scope.remove = function (article) {
      if (article) {
        article.$remove();
        for (var i in $scope.articles) {
          if ($scope.articles[i] === article) {
            $scope.articles.splice(i, 1);
          }
        }
      } else {
        $scope.article.$remove(function () {
          $location.path('articles');
        });
      }
    };
    $scope.update = function () {
      var article = $scope.article;
      article.$update(function () {
        $location.path('articles/' + article._id);
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };
    $scope.find = function () {
      $scope.articles = Articles.query();
    };
    $scope.findOne = function () {
      $scope.article = Articles.get({ articleId: $stateParams.articleId });
    };
  }
]);  // angular.module('articles').directive('hello', function() {
     // 	return {template: 'Hello'};
     // });
'use strict';
//Articles service used for communicating with the articles REST endpoints
angular.module('articles').factory('Articles', [
  '$resource',
  function ($resource) {
    return $resource('articles/:articleId', { articleId: '@_id' }, { update: { method: 'PUT' } });
  }
]);'use strict';
// Setting up route
angular.module('core').config([
  '$stateProvider',
  '$urlRouterProvider',
  function ($stateProvider, $urlRouterProvider) {
    // Redirect to home view when route not found
    $urlRouterProvider.otherwise('/');
    // Home state routing
    $stateProvider.state('home', {
      url: '/',
      templateUrl: 'modules/core/views/home.client.view.html'
    }).state('temple', {
      url: '/t/:temple',
      abstract: true,
      templateUrl: 'modules/core/views/parent.temple.client.view.html'
    }).state('temple.home', {
      url: '/home',
      templateUrl: 'modules/core/views/home.temple.client.view.html'
    });
    ;
  }
]);'use strict';
/*
function setSel2Fields($scope, object){
	if(!$scope.sel2Fields){
		console.log('No $scope.sel2Fields');
		return;
	}

	for(var i=0; i < $scope.sel2Fields.length; i++){
		var selField = $scope.sel2Fields[i];
		if(!person[selField.sub]){
			person[selField.sub] = {};
		}
		if(selField.isTag){
			person[selField.personType][selField.personField] = [];
			for(var i2 = 0; i2 < $scope.selects[selField.local].length; i2++){
				var selItem = $scope.selects[selField.local][i2];
				
				person[selField.personType][selField.personField].push(selItem.id);

			}	
		}else if($scope.selects[selField.local]){
			person[selField.personType][selField.personField] = $scope.selects[selField.local].id;			
		}
	}
}

function getSel2FieldsFromPerson(person, $scope){
	for(var i=0; i < $scope.sel2Fields.length; i++){
		var selField = $scope.sel2Fields[i];
		if($scope.person[selField.personType] && $scope.person[selField.personType][selField.personField]){
			var personValue = $scope.person[selField.personType][selField.personField];
			if(selField.isTag){
				$scope.selects[selField.local] = [];
				for(var i2 = 0; i2 < personValue.length; i2++){
					$scope.selects[selField.local].push({id: personValue[i2], text: personValue[i2]});
				}	
			}else {
				$scope.selects[selField.local] = {id: personValue._id, text: personValue.name.firstName + ' ' + personValue.name.lastName};			
			}	
		
		}
		
	}	
}
*/
angular.module('core').controller('HeaderController', [
  '$scope',
  '$stateParams',
  '$rootScope',
  'Authentication',
  'Menus',
  'Temples',
  function ($scope, $stateParams, $rootScope, Authentication, Menus, Temples) {
    $scope.authentication = Authentication;
    $scope.isCollapsed = false;
    $scope.menu = Menus.getMenu('topbar');
    $rootScope.home = '/#!/';
    $scope.toggleCollapsibleMenu = function () {
      $scope.isCollapsed = !$scope.isCollapsed;
    };
    // Collapsing the menu after navigation
    $scope.$on('$stateChangeSuccess', function () {
      $scope.isCollapsed = false;
    });  // Find existing Temple
         // $scope.findTemple = function() {
         // 	if(!$rootScope.temple){
         // 		if(!$stateParams.temple){
         // 			return;
         // 		}
         // 		$rootScope.temple = Temples.get({ 
         // 		temple: $stateParams.temple
         // 		},function(temple, res){
         // 				$rootScope.home += 't/'+temple.pathname+'/';	
         // 			});	
         // 	}
         // };
  }
]);  // function(temple, res){
     // 				$scope.home += 't/'+temple.pathname;	
     // 			}
'use strict';
angular.module('core').controller('HomeController', [
  '$scope',
  '$rootScope',
  '$stateParams',
  '$location',
  '$timeout',
  'Authentication',
  'Temples',
  'Articles',
  function ($scope, $rootScope, $stateParams, $location, $timeout, Authentication, Temples, Articles) {
    // This provides Authentication context.
    $scope.authentication = Authentication;
    $scope.findTemples = function () {
      if (!$scope.authentication.user) {
        $scope.temples = Temples.query();
        $scope.temples.$promise.then(function (res) {
        });
        $rootScope.home = '/#!/';
        delete $rootScope.temple;
      } else if ($scope.authentication.user.temple) {
        $timeout(function () {
          $location.path('/t/' + $scope.authentication.user.temple.pathname + '/home');
        });
      } else {
        $location.path($rootScope.home.substr(3));
      }
    };
    // Find existing Temple
    $scope.findTemple = function () {
      if (!$rootScope.temple) {
        if ($scope.authentication.user) {
          $rootScope.temple = $scope.authentication.user.temple;
          $rootScope.home = '/#!/t/' + $rootScope.temple.pathname + '/';
          $rootScope.stateHome = 't/' + $rootScope.temple.pathname + '/';
        } else {
          $rootScope.temple = Temples.get({ temple: $stateParams.temple }, function (temple, res) {
            $rootScope.home = '/#!/t/' + temple.pathname + '/';
            $rootScope.stateHome = 't/' + temple.pathname + '/';
          });
        }
      }
    };
  }
]);'use strict';
angular.module('core').directive('addressConverter', function () {
  return {
    require: 'ngModel',
    link: function (scope, elem, attrs, ngModelCtrl) {
      var numBuild;
      ngModelCtrl.$parsers.push(function (googleAddr) {
        // Do to model conversion
        return {
          numberBuilding: numBuild,
          formatted_address: googleAddr.formatted_address,
          lng: googleAddr.geometry.location.lng,
          lat: googleAddr.geometry.location.lat
        };
      });
      ngModelCtrl.$formatters.push(function (dbAddr) {
        if (dbAddr) {
          numBuild = dbAddr.numberBuilding;
        }
        return dbAddr;
      });
    }
  };
});'use strict';
angular.module('core').directive('bsDatefield', function () {
  return {
    require: 'ngModel',
    link: function (scope, element, attrs, ngModelCtrl) {
      if (attrs.bsDatePattern) {
        if (attrs.bsDatePattern.charAt(0) === '/') {
          attrs.bsDatePattern = attrs.bsDatePattern.slice(1, -1);
        }
        var dRegex = new RegExp(attrs.bsDatePattern);
      }
      var dateFormat = attrs.bsDatefield || 'YYYY/MM/DD';
      var isRequired = attrs.required ? true : false;
      ngModelCtrl.$parsers.unshift(function (viewValue) {
        if (Object.prototype.toString.call(viewValue) === '[object Date]') {
          ngModelCtrl.$setValidity('datefield', true);
          return moment(viewValue).format(dateFormat);
        }
        //convert string input into moment data model
        var parsedMoment = moment(viewValue, dateFormat);
        var isValid = dRegex ? parsedMoment.isValid() && dRegex.test(viewValue) : parsedMoment.isValid();
        var parsedDate;
        if (!isValid && !isRequired && viewValue === '') {
          isValid = true;
          parsedDate = '';
        } else {
          parsedDate = parsedMoment.format(dateFormat);
        }
        //toggle validity
        ngModelCtrl.$setValidity('datefield', isValid);
        //return model value
        return isValid ? parsedDate : undefined;
      });  // ngModelCtrl.$formatters.push(function (modelValue) {
           //   var isModelADate = angular.isDate(modelValue) || moment(modelValue).isValid();
           //   ngModelCtrl.$setValidity('datefield', isModelADate);
           //   return isModelADate ? moment(modelValue).format(dateFormat) : undefined;
           // });
    }
  };
});'use strict';
angular.module('core').directive('awDatepickerPattern', function () {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function (scope, elem, attrs, ngModelCtrl) {
      if (attrs.awDatepickerPattern.charAt(0) === '/') {
        attrs.awDatepickerPattern = attrs.awDatepickerPattern.slice(1, -1);
      }
      // else{
      //   regexStr = attrs.awDatepickerPattern;
      // }
      var dRegex = new RegExp(attrs.awDatepickerPattern);
      ngModelCtrl.$parsers.unshift(function (value) {
        if (typeof value === 'string') {
          var isValid = dRegex.test(value);
          ngModelCtrl.$setValidity('datep', isValid);
          if (!isValid) {
            return undefined;
          }
        }
        return value;
      });
    }
  };
});'use strict';
angular.module('core').factory('LookupSelect', [
  'Select2',
  function (Select2) {
    function LookupSelect(baseObj, sub, field, placeHolder, queryFunc, $scope) {
      Select2.call(this, sub, field);
      this.value = '';
      this.scope = $scope;
      this.opts = {
        placeholder: placeHolder,
        allowClear: true,
        minimumInputLength: 2
      };
      if (queryFunc) {
        this.opts.query = queryFunc;
      }
      $scope.$watch('selects.' + field + '.value', this.watchCallback(this, baseObj));
    }
    LookupSelect.prototype = Object.create(Select2.prototype);
    LookupSelect.prototype.constructor = LookupSelect;
    LookupSelect.prototype.setDO = function (objectName) {
      if (!this.value)
        return;
      var watchers = this.scope.$$watchers;
      this.scope.$$watchers = [];
      if (!this.scope[objectName]) {
        this.scope[objectName] = {};
      }
      if (this.sub) {
        if (!this.scope[objectName][this.sub]) {
          this.scope[objectName][this.sub] = {};
        }
        this.scope[objectName][this.sub][this.field] = this.value.id;
      } else {
        this.scope[objectName][this.field] = this.value.id;
      }
      this.scope.$$watchers = watchers;
    };
    LookupSelect.prototype.watchCallback = function (self, objectName) {
      return function () {
        self.setDO(objectName);
      };
    };
    return LookupSelect;
  }
]);'use strict';
angular.module('core').factory('Select2', function () {
  function Select2(sub, field) {
    this.sub = sub;
    this.field = field;
  }
  Select2.setDOs = function (selects, object) {
    for (var select in selects) {
      selects[select].setDO(object);
    }
  };
  Select2.setValues = function (object, selects) {
    for (var select in selects) {
      //selects[select][Object.keys(selects[select])[0]].setValue(object);
      selects[select].setValue(object);
    }
  };
  return Select2;
});'use strict';
angular.module('core').factory('TagSelect', [
  'Select2',
  function (Select2) {
    function TagSelect(baseObj, sub, field, tags, $scope) {
      Select2.call(this, sub, field);
      this.tags = tags;
      this.scope = $scope;
      this.opts = {
        initSelection: function (element, callback) {
          callback(element);
        },
        multiple: true,
        tags: function () {
          return tags;
        },
        width: '100%'
      };
      this.values = [];
      this.new = '';
      this.baseObj = baseObj;
      this.unwatch = false;
      this.scope.$watch('selects.' + this.field + '.values', this.selectWatch(this, this.baseObj));  //this.scope.unwatchBaseObj = this.scope.$watch(baseObj, this.baseObjWatch(this));
    }
    TagSelect.prototype = Object.create(Select2.prototype);
    TagSelect.prototype.constructor = TagSelect;
    TagSelect.prototype.selectWatch = function (self, objectName) {
      return function () {
        self.setDO(objectName);  //console.log('selectWatch self.field:'+self.field);
      };
    };
    TagSelect.prototype.baseObjWatch = function (self) {
      return function () {
        self.setValue(self.scope[self.baseObj]);  //console.log('watch baseObj self.field:'+self.field);
      };
    };
    TagSelect.prototype.setDO = function (baseObj) {
      var i;
      if (!this.scope[baseObj]) {
        this.scope[baseObj] = {};
      }
      if (this.sub) {
        if (!this.scope[baseObj][this.sub]) {
          this.scope[baseObj][this.sub] = {};
        }
        this.scope[baseObj][this.sub][this.field] = [];
        for (i = 0; i < this.values.length; i++) {
          this.scope[baseObj][this.sub][this.field].push(this.values[i].text);
        }
      } else {
        this.scope[baseObj][this.field] = [];
        for (i = 0; i < this.values.length; i++) {
          this.scope[baseObj][this.field].push(this.values[i].text);
        }
      }
    };
    TagSelect.prototype.setValue = function (object) {
      var i;
      if (this.sub) {
        //console.log("setting select for "+this.field);
        if (object[this.sub] && object[this.sub][this.field]) {
          this.values = [];
          //console.log("setting select for "+this.field+" with length:"+object[this.sub][this.field].length+" values.length:"+this.values.length);
          for (i = 0; i < object[this.sub][this.field].length; i++) {
            this.values.push({
              id: object[this.sub][this.field][i],
              text: object[this.sub][this.field][i]
            });
          }
        }
      } else {
        if (object[this.field]) {
          this.values = [];
          //console.log("setting select for "+this.field+" with length:"+object[this.field].length);
          for (i = 0; i < object[this.field].length; i++) {
            this.values.push({
              id: object[this.field][i],
              text: object[this.field][i]
            });
          }
        }
      }
    };
    TagSelect.prototype.addTag = function ($event) {
      $event.preventDefault();
      $event.stopPropagation();
      if (this.new) {
        this.tags.push(this.new);
        this.values.push({
          id: this.new,
          text: this.new
        });
        this.new = '';
      }
    };
    return TagSelect;
  }
]);'use strict';
angular.module('core').filter('saDate', [
  '$filter',
  function ($filter) {
    return function (input) {
      if (input === null) {
        return '';
      }
      var _date = $filter('date')(new Date(input), 'dd/MM/yyyy');
      return _date.toUpperCase();
    };
  }
]);'use strict';
//Menu service used for managing  menus
angular.module('core').service('Menus', [function () {
    // Define a set of default roles
    this.defaultRoles = ['user'];
    // Define the menus object
    this.menus = {};
    // A private function for rendering decision 
    var shouldRender = function (user) {
      if (user) {
        for (var userRoleIndex in user.roles) {
          for (var roleIndex in this.roles) {
            if (this.roles[roleIndex] === user.roles[userRoleIndex]) {
              return true;
            }
          }
        }
      } else {
        return this.isPublic;
      }
      return false;
    };
    // Validate menu existance
    this.validateMenuExistance = function (menuId) {
      if (menuId && menuId.length) {
        if (this.menus[menuId]) {
          return true;
        } else {
          throw new Error('Menu does not exists');
        }
      } else {
        throw new Error('MenuId was not provided');
      }
      return false;
    };
    // Get the menu object by menu id
    this.getMenu = function (menuId) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);
      // Return the menu object
      return this.menus[menuId];
    };
    // Add new menu object by menu id
    this.addMenu = function (menuId, isPublic, roles) {
      // Create the new menu
      this.menus[menuId] = {
        isPublic: isPublic || false,
        roles: roles || this.defaultRoles,
        items: [],
        shouldRender: shouldRender
      };
      // Return the menu object
      return this.menus[menuId];
    };
    // Remove existing menu object by menu id
    this.removeMenu = function (menuId) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);
      // Return the menu object
      delete this.menus[menuId];
    };
    // Add menu item object
    this.addMenuItem = function (menuId, menuItemTitle, menuItemURL, menuItemType, menuItemUIRoute, isPublic, roles) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);
      // Push new menu item
      this.menus[menuId].items.push({
        title: menuItemTitle,
        link: menuItemURL,
        menuItemType: menuItemType || 'item',
        menuItemClass: menuItemType,
        uiRoute: menuItemUIRoute || '/' + menuItemURL,
        isPublic: isPublic || this.menus[menuId].isPublic,
        roles: roles || this.defaultRoles,
        items: [],
        shouldRender: shouldRender
      });
      // Return the menu object
      return this.menus[menuId];
    };
    // Add submenu item object
    this.addSubMenuItem = function (menuId, rootMenuItemURL, menuItemTitle, menuItemURL, menuItemUIRoute, isPublic, roles) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);
      // Search for menu item
      for (var itemIndex in this.menus[menuId].items) {
        if (this.menus[menuId].items[itemIndex].link === rootMenuItemURL) {
          // Push new submenu item
          this.menus[menuId].items[itemIndex].items.push({
            title: menuItemTitle,
            link: menuItemURL,
            uiRoute: menuItemUIRoute || '/' + menuItemURL,
            isPublic: isPublic || this.menus[menuId].isPublic,
            roles: roles || this.defaultRoles,
            shouldRender: shouldRender
          });
        }
      }
      // Return the menu object
      return this.menus[menuId];
    };
    // Remove existing menu object by menu id
    this.removeMenuItem = function (menuId, menuItemURL) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);
      // Search for menu item to remove
      for (var itemIndex in this.menus[menuId].items) {
        if (this.menus[menuId].items[itemIndex].link === menuItemURL) {
          this.menus[menuId].items.splice(itemIndex, 1);
        }
      }
      // Return the menu object
      return this.menus[menuId];
    };
    // Remove existing menu object by menu id
    this.removeSubMenuItem = function (menuId, submenuItemURL) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);
      // Search for menu item to remove
      for (var itemIndex in this.menus[menuId].items) {
        for (var subitemIndex in this.menus[menuId].items[itemIndex].items) {
          if (this.menus[menuId].items[itemIndex].items[subitemIndex].link === submenuItemURL) {
            this.menus[menuId].items[itemIndex].items.splice(subitemIndex, 1);
          }
        }
      }
      // Return the menu object
      return this.menus[menuId];
    };
    //Adding the topbar menu
    this.addMenu('topbar');
  }]);'use strict';
// Configuring the Articles module
angular.module('people').run([
  'Menus',
  function (Menus) {
    // Set top bar menu items
    Menus.addMenuItem('topbar', 'People', 'people', 'dropdown', '/people(/create)?', false, [
      'systemAdmin',
      'templeAdmin',
      'peopleAdmin',
      'peopleUser'
    ]);
    Menus.addSubMenuItem('topbar', 'people', 'List People', 'people');
    Menus.addSubMenuItem('topbar', 'people', 'New Person', 'people/create', '/people/create', false, [
      'systemAdmin',
      'templeAdmin',
      'peopleAdmin'
    ]);
  }
]);'use strict';
//Setting up route
angular.module('people').config([
  '$stateProvider',
  function ($stateProvider) {
    // People state routing
    $stateProvider.state('temple.listPeople', {
      url: '/people',
      templateUrl: 'modules/people/views/list-people.client.view.html'
    }).state('temple.createPerson', {
      url: '/people/create',
      templateUrl: 'modules/people/views/create-person.client.view.html'
    }).state('temple.viewPerson', {
      url: '/people/:personId',
      templateUrl: 'modules/people/views/view-person.client.view.html'
    }).state('temple.editPerson', {
      url: '/people/:personId/edit',
      templateUrl: 'modules/people/views/edit-person.client.view.html'
    });
  }
]);'use strict';
var ModalInstanceCtrl = function ($scope, $modalInstance, comment) {
  $scope.comment = {};
  $scope.comment.comment = comment;
  $scope.cancelComment = function () {
    $modalInstance.dismiss('cancel');
  };
  $scope.saveComment = function () {
    $modalInstance.close($scope.comment.comment);
  };
};
// People controller
angular.module('people').controller('PeopleController', [
  '$scope',
  '$stateParams',
  '$location',
  '$window',
  '$rootScope',
  '$timeout',
  '$modal',
  'Authentication',
  'People',
  'dialogs',
  '$http',
  'uiGridConstants',
  function ($scope, $stateParams, $location, $window, $rootScope, $timeout, $modal, Authentication, People, $dialogs, $http, uiGridConstants) {
    //defaults
    $scope.person = {};
    $scope.comment = {};
    $scope.person.sex = 'Male';
    $scope.person.race = 'Indian';
    $scope.authentication = Authentication;
    // Create new Person
    $scope.create = function () {
      // Create new Person object
      var person = new People(this.person);
      if (this.comment.comment) {
        if ($scope.authentication.user.person) {
          this.comment.person = $scope.authentication.user.person._id;
        }
        this.comment.date = new Date();
        person.comments = [];
        person.comments.push(this.comment);
      }
      // Redirect after save
      person.$save({ temple: $stateParams.temple }, function (response) {
        $location.path('t/' + $stateParams.temple + '/people');
      }, function (errorResponse) {
        $scope.errors = errorResponse.data;
        $window.scrollTo(0, 0);
      });  // Clear form fields
           //this.name = '';
    };
    // Remove existing Person
    $scope.remove = function (person) {
      if (person) {
        person.$remove({ temple: $stateParams.temple }, function (res) {
          for (var i in $scope.people) {
            if ($scope.people[i] === person) {
              $scope.people.splice(i, 1);
            }
          }
        }, function (err) {
          $scope.errors = err.data;
        });
      } else {
        $scope.person.$remove(function () {
          $location.path('people');
        });
      }
    };
    // Update existing Person
    $scope.update = function () {
      var person = $scope.person;
      person.$update({ temple: $stateParams.temple }, function () {
        $location.path('t/' + $stateParams.temple + '/people');
      }, function (errorResponse) {
        console.error('errors:' + errorResponse.data);
        $scope.errors = errorResponse.data;
        $window.scrollTo(0, 0);
      });
    };
    // Find a list of People
    $scope.find = function () {
      if (!$scope.people) {
        $scope.people = People.query({ temple: $stateParams.temple });
      }
    };
    // Find existing Person
    $scope.findOne = function () {
      $scope.person = People.get({
        personId: $stateParams.personId,
        temple: $stateParams.temple
      }, function (person, res) {
        for (var i = 0; person.comments && i < person.comments.length; i++) {
          if ($scope.authentication.user.person && person.comments[i].person) {
            person.comments[i].disabled = $scope.authentication.user.person._id !== person.comments[i].person._id;
          } else {
            person.comments[i].disabled = false;
          }
        }
      });
    };
    $scope.findDevotees = function () {
      if (!$scope.devotees) {
        $scope.devotees = People.query({
          temple: $stateParams.temple,
          initiatedLevel: 2
        });
      }
    };
    $scope.findGurus = function () {
      if (!$scope.gurus) {
        $scope.gurus = People.query({
          temple: $stateParams.temple,
          initiatedLevel: 5
        });
      }
    };
    $scope.datePickers = {
      regex: /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$|\A\Z/,
      birthday: {
        default: '2000-01-01',
        open: false
      },
      lastContact: {
        open: false,
        maxDate: new Date()
      },
      dateMovedIn: {
        open: false,
        maxDate: new Date()
      },
      comment: {
        open: false,
        maxDate: new Date()
      }
    };
    $scope.openDate = function ($event, field) {
      $event.preventDefault();
      $event.stopPropagation();
      if ($scope.datePickers[field].default) {
        this.person[field] = $scope.datePickers[field].default;
      }
      $scope.datePickers[field].open = true;
    };
    $scope.refreshAddresses = function (address) {
      var params = {
          address: address,
          sensor: false,
          components: 'country:ZA'
        };
      return $http.get('http://maps.googleapis.com/maps/api/geocode/json', { params: params }).then(function (response) {
        $scope.addresses = response.data.results;
      });
    };
    $scope.selects = {
      skills: [
        'Kirtana',
        'Class',
        'Cooking',
        'IT'
      ],
      medicalConditions: [
        'Asthma',
        'Bee Sting Allergy',
        'Diabetes',
        'HIV',
        'Gluten Allergy',
        'Heart Disease'
      ],
      participation: [
        'Attends service',
        'Diety Seva',
        'Misc Seva',
        'Donor',
        'Outreach',
        'Management'
      ],
      courses: [
        'Bhakti-Sadacara',
        'Bhakti-Sastri'
      ]
    };
    $scope.grid = {};
    $scope.grid.comment = {
      data: 'person.comments',
      enableColumnResizing: true,
      enableRowSelection: true,
      multiSelect: false,
      enableHorizontalScrollbar: uiGridConstants.scrollbars.NEVER,
      rowHeight: 45,
      columnDefs: [
        {
          field: 'date',
          displayName: 'Date',
          cellTemplate: '<div class="ngCellText">{{row.entity.date | saDate}}</div>',
          visible: !$scope.authentication.isMobile()
        },
        {
          field: 'comment',
          displayName: 'Comment',
          width: '50%'
        },
        {
          field: 'person.displayName',
          displayName: 'Commentor',
          visible: !$scope.authentication.isMobile()
        },
        {
          name: 'actions',
          displayName: '',
          cellTemplate: '<div class="ngCellText"  data-ng-model="row" )">' + '<button ng-disabled="row.entity.disabled" ng-click="grid.appScope.commentGrdScp.editRowComment(row,$event)">' + '<i class="glyphicon glyphicon-edit"/></button></div>',
          width: '10%'
        }
      ]
    };
    $scope.openComment = function ($event) {
      $event.preventDefault();
      $event.stopPropagation();
      var modalInstance = $modal.open({
          templateUrl: 'modules/people/views/crud-comment.client.view.html',
          controller: function ($scope, $modalInstance) {
            $scope.comment = {};
            $scope.cancelComment = function () {
              $modalInstance.dismiss('cancel');
            };
            $scope.saveComment = function () {
              $modalInstance.close($scope.comment.comment);
            };
          }
        });
      modalInstance.result.then(function (comment) {
        if (!$scope.person.comments) {
          $scope.person.comments = [];
        }
        $scope.person.comments.push({
          date: new Date(),
          person: $scope.authentication.user.person,
          comment: comment
        });
      });
    };
    $scope.peopleGrdScp = {
      removeConfirm: function (person) {
        var dlg = $dialogs.confirm('Please confirm', 'Would you like to delete ' + (person ? person.displayName : $scope.person.displayName) + '?');
        dlg.result.then(function (btn) {
          $scope.remove(person);
        });
      },
      authentication: Authentication,
      home: $rootScope.home
    };
    $scope.commentGrdScp = {
      editRowComment: function (row, $event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.comRow = row;
        var modalInstance = $modal.open({
            templateUrl: 'modules/people/views/crud-comment.client.view.html',
            controller: ModalInstanceCtrl,
            resolve: {
              comment: function () {
                return $scope.comRow.entity.comment;
              }
            }
          });
        modalInstance.result.then(function (comment) {
          $scope.comRow.entity.comment = comment;  //$scope.person.comments[$scope.comRow.rowIndex].comment = comment;
        });
      }
    };
    $scope.grid.list = {
      data: 'people',
      enableColumnResize: true,
      enableRowSelection: false,
      rowHeight: 45,
      showFooter: false,
      enableFiltering: true,
      showColumnMenu: false,
      filterOptions: { filterText: '' },
      columnDefs: [
        {
          field: 'displayName',
          displayName: 'Name'
        },
        {
          field: 'mobileNumber',
          displayName: 'Mobile'
        },
        {
          field: 'homeNumber',
          displayName: 'Home',
          visible: !$scope.authentication.isMobile()
        },
        {
          field: 'workNumber',
          displayName: 'Work',
          visible: !$scope.authentication.isMobile()
        },
        {
          field: 'email',
          displayName: 'Email'
        },
        {
          name: 'action',
          displayName: 'Action',
          cellTemplate: '<div class="ngCellText"  data-ng-model="row" )">' + '<button ng-disabled="row.entity.disabled" go-click="{{grid.appScope.home}}people/{{row.entity._id}}/edit">' + '<i class="glyphicon glyphicon-edit"/></button>' + '<button ng-disabled="row.entity.disabled" ' + 'ng-show="grid.appScope.authentication.isAuthorized([&quot;systemAdmin&quot;,&quot;templeAdmin&quot;,&quot;peopleAdmin&quot;])" ' + 'ng-click="grid.appScope.peopleGrdScp.removeConfirm(row.entity)">' + '<i class="glyphicon glyphicon-trash"/></button>' + '</div>'
        }
      ]
    };
  }
]);'use strict';
angular.module('core').factory('PeopleLookup', [
  'LookupSelect',
  'People',
  function (LookupSelect, People) {
    function PeopleLookup(objectName, sub, field, placeHolder, templePath, initLevel, $scope) {
      LookupSelect.call(this, objectName, sub, field, placeHolder, null, $scope);
      this.initLevel = initLevel;
      this.opts = {
        placeholder: placeHolder,
        allowClear: true,
        minimumInputLength: 2,
        query: function (query) {
          var data = { results: [] };
          People.query({
            temple: templePath,
            initiatedLevel: initLevel
          }, function (people, res) {
            for (var i = 0; i < people.length; i++) {
              data.results.push({
                id: people[i]._id,
                text: people[i].displayName
              });
            }
            query.callback(data);
          });
        }
      };
    }
    PeopleLookup.prototype = Object.create(LookupSelect.prototype);
    PeopleLookup.prototype.constructor = PeopleLookup;
    //takes the selected person value from the root person object and sets the name and id on the control
    PeopleLookup.prototype.setValue = function (person) {
      var selPerson;
      if (this.sub && person[this.sub]) {
        selPerson = person[this.sub][this.field];
      } else {
        selPerson = person[this.field];
      }
      if (selPerson) {
        this.value = {
          id: selPerson._id,
          text: selPerson.displayName
        };
      } else {
        this.value = null;
      }
    };
    return PeopleLookup;
  }
]);'use strict';
//People service used to communicate People REST endpoints
angular.module('people').factory('People', [
  '$resource',
  function ($resource) {
    return $resource('t/:temple/people/:personId', {
      personId: '@_id',
      temple: '@temple.pathname'
    }, { update: { method: 'PUT' } });
  }
]);'use strict';
// Configuring the Articles module
angular.module('temples').run([
  'Menus',
  function (Menus) {
    // Set top bar menu items
    Menus.addMenuItem('topbar', 'Temples', 'temples', 'dropdown', '/temples(/create)?', false, ['systemAdmin']);
    Menus.addSubMenuItem('topbar', 'temples', 'List Temples', 'temples');
    Menus.addSubMenuItem('topbar', 'temples', 'New Temple', 'temples/create');
  }
]);'use strict';
//Setting up route
angular.module('temples').config([
  '$stateProvider',
  function ($stateProvider) {
    // Temples state routing
    $stateProvider.state('listTemples', {
      url: '/temples',
      templateUrl: 'modules/temples/views/list-temples.client.view.html'
    }).state('createTemple', {
      url: '/temples/create',
      templateUrl: 'modules/temples/views/create-temple.client.view.html'
    }).state('editTemple', {
      url: '/temples/:templeId',
      templateUrl: 'modules/temples/views/edit-temple.client.view.html'
    });
  }
]);'use strict';
// Temples controller
angular.module('temples').controller('TemplesController', [
  '$scope',
  '$stateParams',
  '$location',
  'Authentication',
  'CrudTemples',
  'dialogs',
  function ($scope, $stateParams, $location, Authentication, CrudTemples, $dialogs) {
    $scope.authentication = Authentication;
    $scope.temple = {};
    // Create new Temple
    $scope.create = function () {
      // Create new Temple object
      var temple = new CrudTemples($scope.temple);
      // var temple = new Temples ({
      // 	name: this.name,
      // 	address: {line1: this.line1, line2: this.line2, line3: this.line3}
      // });
      // Redirect after save
      temple.$save(function (response) {
        $location.path('temples');
      }, function (errorResponse) {
        $scope.errors = errorResponse.data;
      });  // Clear form fields
           //this.name = '';
    };
    // Remove existing Temple
    $scope.removeConfirm = function (temple) {
      var dlg = $dialogs.confirm('Please confirm', 'Would you like to delete ' + (temple ? temple.name : $scope.temple.name) + '?');
      dlg.result.then(function (btn) {
        $scope.remove(temple);
      });
    };
    $scope.remove = function (temple) {
      if (temple) {
        temple.$remove(function (res) {
          for (var i in $scope.temples) {
            if ($scope.temples[i] === temple) {
              $scope.temples.splice(i, 1);
            }
          }
        }, function (err) {
          $scope.errors = err.data;
        });
      } else {
        $scope.temple.$remove(function () {
          $location.path('temples');
        });
      }
    };
    // Update existing Temple
    $scope.update = function () {
      var temple = $scope.temple;
      temple.$update(function () {
        $location.path('temples');
      }, function (errorResponse) {
        $scope.errors = errorResponse.data;
      });
    };
    // Find a list of Temples
    $scope.find = function () {
      $scope.temples = CrudTemples.query();
    };
    // Find existing Temple
    $scope.findOne = function () {
      $scope.temple = CrudTemples.get({ templeId: $stateParams.templeId });
    };
  }
]);'use strict';
//Temples service used to communicate Temples REST endpoints
angular.module('temples').factory('Temples', [
  '$resource',
  function ($resource) {
    return $resource('t/:temple', { temple: '@pathname' });
  }
]);
angular.module('temples').factory('CrudTemples', [
  '$resource',
  function ($resource) {
    return $resource('temples/:templeId', { templeId: '@_id' }, { update: { method: 'PUT' } });
  }
]);'use strict';
// Config HTTP Error Handling
angular.module('users').config([
  '$httpProvider',
  function ($httpProvider) {
    // Set the httpProvider "not authorized" interceptor
    $httpProvider.interceptors.push([
      '$q',
      '$location',
      'Authentication',
      function ($q, $location, Authentication) {
        return {
          responseError: function (rejection) {
            switch (rejection.status) {
            case 401:
              // Deauthenticate the global user
              Authentication.user = null;
              // Redirect to signin page
              $location.path('signin');
              break;
            case 403:
              // Add unauthorized behaviour 
              break;
            }
            return $q.reject(rejection);
          }
        };
      }
    ]);
  }
]);
// Configuring the Articles module
angular.module('users').run([
  'Menus',
  function (Menus) {
    // Set top bar menu items
    Menus.addMenuItem('topbar', 'Users', 'users', 'dropdown', '/users(/create)?', false, [
      'systemAdmin',
      'templeAdmin'
    ]);
    Menus.addSubMenuItem('topbar', 'users', 'List Users', 'users');
    Menus.addSubMenuItem('topbar', 'users', 'New Users', 'users/create');
  }
]);'use strict';
// Setting up route
angular.module('users').config([
  '$stateProvider',
  function ($stateProvider) {
    // Users state routing
    $stateProvider.state('password', {
      url: '/settings/password',
      templateUrl: 'modules/users/views/settings/change-password.client.view.html'
    }).state('signin', {
      url: '/signin',
      templateUrl: 'modules/users/views/signin.client.view.html'
    }).state('signup', {
      url: '/signup',
      templateUrl: 'modules/users/views/signup.client.view.html'
    }).state('temple.profile', {
      url: '/settings/profile',
      templateUrl: 'modules/users/views/settings/edit-profile.client.view.html'
    }).state('temple.password', {
      url: '/settings/password',
      templateUrl: 'modules/users/views/settings/change-password.client.view.html'
    }).state('temple.accounts', {
      url: '/settings/accounts',
      templateUrl: 'modules/users/views/settings/social-accounts.client.view.html'
    }).state('temple.signin', {
      url: '/signin',
      templateUrl: 'modules/users/views/signin.client.view.html'
    }).state('listUsers', {
      url: '/users',
      templateUrl: 'modules/users/views/list-users.client.view.html'
    }).state('createUser', {
      url: '/users/create',
      templateUrl: 'modules/users/views/create-users.client.view.html'
    }).state('editUser', {
      url: '/users/:userId',
      templateUrl: 'modules/users/views/edit-users.client.view.html'
    }).state('temple.listUsers', {
      url: '/users',
      templateUrl: 'modules/users/views/list-users.client.view.html'
    }).state('temple.createUser', {
      url: '/users/create',
      templateUrl: 'modules/users/views/create-users.client.view.html'
    }).state('temple.editUser', {
      url: '/users/:userId',
      templateUrl: 'modules/users/views/edit-users.client.view.html'
    });
    ;
  }
]);'use strict';
angular.module('users').controller('AuthenticationController', [
  '$scope',
  '$http',
  '$location',
  '$stateParams',
  'Authentication',
  function ($scope, $http, $location, $stateParams, Authentication) {
    $scope.authentication = Authentication;
    //If user is signed in then redirect back home
    if ($scope.authentication.user)
      $location.path('/');
    $scope.signup = function () {
      $http.post('/auth/signup', $scope.credentials).success(function (response) {
        //If successful we assign the response to the global user model
        $scope.authentication.user = response;
        //And redirect to the index page
        $location.path('/');
      }).error(function (response) {
        $scope.error = response.message;
      });
    };
    $scope.signin = function () {
      var signinUrl = '/auth/signin';
      var indexUrl = '/';
      if ($stateParams.temple) {
        signinUrl = '/t/' + $stateParams.temple + signinUrl;
        indexUrl = '/t/' + $stateParams.temple + '/home';
      }
      $http.post(signinUrl, $scope.credentials).success(function (response) {
        //If successful we assign the response to the global user model
        $scope.authentication.user = response;
        window.user = response;
        //And redirect to the index page
        $location.path(indexUrl);
      }).error(function (response) {
        $scope.error = response.message;
      });
    };
  }
]);'use strict';
angular.module('users').controller('SettingsController', [
  '$scope',
  '$http',
  '$location',
  '$stateParams',
  'Users',
  'Authentication',
  function ($scope, $http, $location, $stateParams, Users, Authentication) {
    $scope.user = Authentication.user;
    // If user is not signed in then redirect back home
    if (!$scope.user)
      $location.path('/');
    // Check if there are additional accounts 
    // $scope.hasConnectedAdditionalSocialAccounts = function(provider) {
    // 	for (var i in $scope.user.additionalProvidersData) {
    // 		return true;
    // 	}
    // 	return false;
    // };
    // Check if provider is already in use with current user
    // $scope.isConnectedSocialAccount = function(provider) {
    // 	return $scope.user.provider === provider || ($scope.user.additionalProvidersData && $scope.user.additionalProvidersData[provider]);
    // };
    // Remove a user social account
    // $scope.removeUserSocialAccount = function(provider) {
    // 	$scope.success = $scope.error = null;
    // 	$http.delete('/users/accounts', {
    // 		params: {
    // 			provider: provider
    // 		}
    // 	}).success(function(response) {
    // 		// If successful show success message and clear form
    // 		$scope.success = true;
    // 		$scope.user = Authentication.user = response;
    // 	}).error(function(response) {
    // 		$scope.error = response.message;
    // 	});
    // };
    // Update a user profile
    // $scope.updateUserProfile = function() {
    // 	$scope.success = $scope.error = null;
    // 	var user = new Users($scope.user);
    // 	user.$update(function(response) {
    // 		$scope.success = true;
    // 		Authentication.user = response;
    // 	}, function(response) {
    // 		$scope.error = response.data.message;
    // 	});
    // };
    // Change user password
    $scope.changeUserPassword = function () {
      $scope.success = $scope.error = null;
      $http.post(($stateParams.temple ? 't/' + $stateParams.temple + '/' : '') + 'user/password', $scope.passwordDetails).success(function (response) {
        // If successful show success message and clear form
        $scope.success = true;
        $scope.passwordDetails = null;
      }).error(function (response) {
        $scope.error = response.message;
      });
    };
  }
]);'use strict';
function setScopeRoles($scope, user) {
  for (var i = 0; i < user.roles.length; i++) {
    switch (user.roles[i]) {
    case 'peopleUser':
      $scope.roles.peopleUser = true;
      break;
    case 'peopleAdmin':
      $scope.roles.peopleAdmin = true;
      break;
    case 'templeAdmin':
      $scope.roles.templeAdmin = true;
      break;
    case 'systemAdmin':
      $scope.roles.systemAdmin = true;
      break;
    }
  }
}
function getScopeRoles($scope) {
  var rolesArray = [];
  if ($scope.roles.peopleUser)
    rolesArray.push('peopleUser');
  if ($scope.roles.peopleAdmin)
    rolesArray.push('peopleAdmin');
  if ($scope.roles.templeAdmin)
    rolesArray.push('templeAdmin');
  if ($scope.roles.systemAdmin)
    rolesArray.push('systemAdmin');
  return rolesArray;
}
angular.module('users').controller('UsersController', [
  '$scope',
  '$http',
  '$location',
  '$stateParams',
  '$rootScope',
  'Authentication',
  'Users',
  'TempleUsers',
  'Temples',
  'PeopleLookup',
  'Select2',
  'dialogs',
  'People',
  function ($scope, $http, $location, $stateParams, $rootScope, Authentication, Users, TempleUsers, Temples, PeopleLookup, Select2, $dialogs, People) {
    $scope.authentication = Authentication;
    $scope.roles = {
      peopleUser: false,
      peopleAdmin: false,
      templeAdmin: false,
      systemAdmin: false
    };
    $scope.crudUser = {};
    $scope.form = { temple: $stateParams.temple };
    $scope.findDevotees = function () {
      if (!$scope.devotees) {
        $scope.devotees = People.query({
          temple: $stateParams.temple,
          initiatedLevel: 1
        });
      }
    };
    //$scope.form.temple = $stateParams.temple;
    //$scope.test = $stateParams.temple;
    // Create new Temple
    $scope.create = function () {
      var user;
      if ($scope.crudUser.password !== $scope.form.password2) {
        $scope.errors = ['Passwords don\'t match'];
        return;
      }
      // Create new Temple object
      // var user = new Users ({
      // 	email: this.email,
      // 	password: this.password,
      // 	roles: getScopeRoles($scope),
      // 	temple: this.userTemple
      // });
      if ($stateParams.temple) {
        user = new TempleUsers(this.crudUser);
      } else {
        user = new Users(this.crudUser);
      }
      user.roles = getScopeRoles($scope);
      // Redirect after save
      user.$save({ temple: $stateParams.temple }, function (response) {
        $location.path(($stateParams.temple ? 't/' + $stateParams.temple + '/' : '') + 'users');
      }, function (errorResponse) {
        $scope.errors = errorResponse.data;
      });
    };
    // Remove existing User
    $scope.removeConfirm = function (user) {
      var dlg = $dialogs.confirm('Please confirm', 'Would you like to delete ' + (user ? user.email : $scope.user.email) + '?');
      dlg.result.then(function (btn) {
        $scope.remove(user);
      });
    };
    $scope.remove = function (user) {
      if (user) {
        user.$remove({ temple: $stateParams.temple });
        for (var i in $scope.users) {
          if ($scope.users[i] === user) {
            $scope.users.splice(i, 1);
          }
        }
      } else {
        $scope.user.$remove({ temple: $stateParams.temple }, function () {
          $location.path(($stateParams.temple ? 't/' + $stateParams.temple + '/' : '') + 'users');
        });
      }
    };
    // Find a User
    $scope.find = function () {
      if ($stateParams.temple) {
        $scope.users = TempleUsers.query({ temple: $stateParams.temple });
      } else {
        $scope.users = Users.query();
      }
    };
    // Find temples
    $scope.findTemples = function () {
      if (!$scope.temples && !$scope.temple) {
        $scope.temples = Temples.query();
      }
    };
    // Update existing User
    $scope.update = function () {
      var user = $scope.crudUser;
      user.roles = getScopeRoles($scope);
      if ($scope.crudUser.password !== $scope.form.password2) {
        $scope.errors = ['Passwords don\'t match'];
        return;
      }
      if ($scope.initPass === user.password) {
        delete user.password;
      }
      user.$update({ temple: $stateParams.temple }, function () {
        $location.path(($stateParams.temple ? 't/' + $stateParams.temple + '/' : '') + 'users');
      }, function (errorResponse) {
        $scope.errors = errorResponse.data;
        $scope.crudUser.password = $scope.initPass;
        $scope.form2.password2 = $scope.initPass;
      });
    };
    // Find existing User
    $scope.findOne = function () {
      var findCallBack = function (user, res) {
        setScopeRoles($scope, user);
        $scope.password2 = user.password;
        $scope.initPass = user.password;  //Select2.setValues(user, $scope.selects);
      };
      if ($stateParams.temple) {
        $scope.crudUser = TempleUsers.get({
          userId: $stateParams.userId,
          temple: $stateParams.temple
        }, findCallBack);
      } else {
        $scope.crudUser = Users.get({ userId: $stateParams.userId }, findCallBack);
      }
    };  // $scope.selects = {
        // 	person:  new PeopleLookup('crudUser','','person','Choose a person', $stateParams.temple, 1,$scope)
        // };
        //relationshipManager: new PeopleLookup('person','contact', 'relationshipManager', 'Choose a person', $stateParams.temple, 2, $scope),
  }
]);  //testing
'use strict';
angular.module('users').directive('confirmation', function () {
  return {
    restrict: 'A',
    link: function (scope, element, attr) {
      scope.confirmObject = attr.confirmObject;
      angular.element(element).confirmation();  //$('[data-toggle="confirmation"]').confirmation();
    }  //			template: 'Hello'
  };
});'use strict';
angular.module('users').directive('goClick', [
  '$location',
  function ($location) {
    return function (scope, element, attrs) {
      var path;
      attrs.$observe('goClick', function (val) {
        path = val;
      });
      element.bind('click', function () {
        scope.$apply(function () {
          if (path.indexOf('/#!') !== -1) {
            path = path.substr(3);
          }
          $location.path(path);  //scope.apply();
        });
      });
    };
  }
]);'use strict';
// Authentication service for user variables
angular.module('users').factory('Authentication', [
  '$stateParams',
  '$location',
  function ($stateParams, $location) {
    if (window.user && window.user.temple && $location.url().indexOf(window.user.temple.pathname) === -1) {
      if (!$stateParams.temple) {
        //console.error('resetting temple window.user.temple:'+window.user.temple.pathname+' $location.url()'+$location.url()+' $stateParams:'+JSON.stringify($stateParams));
        $location.path('t/' + window.user.temple.pathname + $location.url());
      } else if (window.user.temple.pathname !== $stateParams.temple) {
        //console.error('logging out');
        location.pathname = '/t/' + window.user.temple.pathname + '/auth/signout';
      }
    }
    var _this = this;
    //var temple = $stateProvider.temple;
    _this._data = {
      user: window.user,
      isAuthorized: function (roles) {
        var matchedRoles = roles.filter(function (n) {
            //console.log(window.user);
            return window.user.roles.indexOf(n) !== -1;
          });
        return matchedRoles.length > 0;
      },
      isMobile: function () {
        var isMobile = false;
        (function (a) {
          if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4)))
            isMobile = true;
        }(navigator.userAgent || navigator.vendor || window.opera));
        return isMobile;
      }
    };
    return _this._data;
  }
]);  //d
'use strict';
// Users service used for communicating with the users REST endpoint
angular.module('users').factory('Users', [
  '$resource',
  function ($resource) {
    return $resource('users/:userId', { userId: '@_id' }, { update: { method: 'PUT' } });
  }
]);
angular.module('users').factory('TempleUsers', [
  '$resource',
  function ($resource) {
    return $resource('/t/:temple/users/:userId', {
      userId: '@_id',
      temple: '@temple.pathname'
    }, { update: { method: 'PUT' } });
  }
]);