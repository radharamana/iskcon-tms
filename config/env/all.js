'use strict';

module.exports = {
	app: {
		title: 'ISKCON - TMS',
		description: 'ISKCON Temple Management System',
		keywords: 'ISKCON, CRM, MongoDB, Express, AngularJS, Node.js'
	},
	port: process.env.PORT || 3000,
	templateEngine: 'swig',
	sessionSecret: 'GOURA@108',
	sessionCollection: 'sessions',
	assets: {
		lib: {
			css: [
				'public/lib/bootstrap/dist/css/bootstrap.css',
				'public/lib/bootstrap/dist/css/bootstrap-theme.css',
				//'public/lib/select2/select2.css',
				'public/lib/angular-ui-grid/ui-grid.css',
				'public/lib/angular-dialog-service/src/dialogs.css',
				'public/lib/angular-ui-select/dist/select.css'
			],
			js: [
				//'public/lib/jquery/dist/jquery.min.js',
				//'public/lib/bootstrap/dist/js/bootstrap.min.js',
				'public/lib/angular/angular.js',
				'public/lib/angular-resource/angular-resource.js', 
				'public/lib/angular-cookies/angular-cookies.js', 
				'public/lib/angular-animate/angular-animate.js', 
				'public/lib/angular-touch/angular-touch.js', 
				'public/lib/angular-sanitize/angular-sanitize.js', 
				'public/lib/angular-ui-router/release/angular-ui-router.js',
				'public/lib/angular-ui-utils/ui-utils.js',
				'public/lib/angular-bootstrap/ui-bootstrap-tpls.js',
				'public/lib/angular-translate/angular-translate.js',
				//'public/lib/select2/select2.js',
				//'public/lib/angular-ui-select2/src/select2.js',
				'public/lib/angular-ui-grid/ui-grid.js',
				'public/lib/angular-dialog-service/dist/dialogs.min.js',
				'public/lib/angular-dialog-service/dist/dialogs-default-translations.min.js',
				'public/lib/moment/moment.js',
				'public/lib/angular-ui-select/dist/select.js'
				
			]
		},
		css: [
			'public/modules/**/css/*.css'
		],
		js: [
			'public/config.js',
			'public/application.js',
			'public/modules/*/*.js',
			'public/modules/*/*[!tests]*/*.js'
		],
		tests: [
			'public/lib/angular-mocks/angular-mocks.js',
			'public/modules/*/tests/*.js'
		],
		testsE2e:[
			'app/tests-e2e/*.js'
		]

	}
};