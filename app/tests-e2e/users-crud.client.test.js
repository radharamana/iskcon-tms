'use strict';

var e2e = require('./e2e.js');
var Promise = require('promise');
var async = require('async');
var util = require('util');
var userPage = require('./users-crud.client.pages.js');
var homePage = require('./home.client.page.js');


var getUserInList = function(email){
	//console.log('getUserInList email:'+email);
	return element.all(by.repeater('user in users')).filter(function(el, index){
		//console.log('index:'+index);
		return el.getText().then(function(text){
			//console.log('text:'+text+' email:'+email);
			return text === email;
		});
	}).then(function(els){
		//console.log('els length'+els.length+'els[0]'+els[0]);
		if(els.length === 0)return null;
		return els[0];
	});
};
// var getMenuItemEl = function(itemName) {
//     return element.all(by.repeater('item in menu.items'))
//     .then(function(els){
//         return Promise.all(els.map(function(el) {
//             return el.getText()
//             	.then(function(text){
//                 	console.log('getMenuItemEl:'+text);
//                 	return (text === itemName) ? el : null;
//             	});
//         }))
//         .then(function(foundEls) {
//             console.log('here');
//             for (var i=0; i<foundEls.length; i++)
//                 if (foundEls[i] !== null) {
//                     console.log('fulfill');
//                     return foundEls[i];
//                 }
//             throw new Error('no item found');
//         });
//     });
// };




describe('User CRUD on Root E2E tests', function(){
	it('init', function(){
		browser.driver.manage().deleteAllCookies();
		homePage.go();
		homePage.signin('rory.gilfillan@gmail.com');
	});

	it('test root authorization', function(){
		userPage.go('users');
		expect(userPage.isDelete(userPage.firstUser)).toBe(true);

	});

	it('Create a user as root', function(){
		userPage.go('users','create');
		expect(browser.getLocationAbsUrl()).toContain('/users/create');
		expect(userPage.header.getText()).toBe('New User');

		expect(userPage.isPerson).toBe(false);

		userPage.email.sendKeys('test2@test.com');
		userPage.password.sendKeys('password');
		userPage.password2.sendKeys('password');
		userPage.peopleUser.click();
		userPage.peopleAdmin.click();
		userPage.templeAdmin.click();
		userPage.systemAdmin.click();
		userPage.temple.sendKeys('Lenasia');
		userPage.save.click();

		expect(userPage.header.getText()).toBe('Users');
		expect(userPage.firstUser.getText()).toBe('test2@test.com');
		
	});

	it('Edit a user', function(){
		userPage.edit(userPage.firstUser);

		expect(userPage.header.getText()).toBe('Edit User');

		expect(userPage.email.getAttribute('value')).toBe('test2@test.com');
		expect(userPage.peopleUser.getAttribute('class')).toContain('active');
		expect(userPage.peopleAdmin.getAttribute('class')).toContain('active');
		expect(userPage.templeAdmin.getAttribute('class')).toContain('active');
		expect(userPage.systemAdmin.getAttribute('class')).toContain('active');
		expect(userPage.templeSel.getText()).toBe('Lenasia');

		userPage.email.clear();
		userPage.email.sendKeys('test3@test.com');
		userPage.password.clear();
		userPage.password.sendKeys('password1');
		userPage.password2.sendKeys('password1');
		userPage.peopleUser.click();
		userPage.save.click();

		expect(userPage.header.getText()).toBe('Users');
		
	});

	it('Login with user', function(){
		e2e.signout();
		e2e.selectTemple();
		e2e.login('test3@test.com','password1');
	});

	it('Check authorization user', function(){
		expect(browser.getLocationAbsUrl()).toContain('lenasia');
		expect(e2e.getMenuItemEl('Temples')).not.toBeNull();
		expect(e2e.getMenuItemEl('Users')).not.toBeNull();
		expect(e2e.getMenuItemEl('People')).not.toBeNull();

		userPage.go('t/lenasia','users');
		expect(userPage.isDelete(userPage.firstUser)).toBe(true);

		userPage.go('t/lenasia','users','create');
		expect(browser.getLocationAbsUrl()).toContain('lenasia');
		expect(userPage.isPerson).toBe(true);
		expect(userPage.isTemple).toBe(false);
	});

	it('Create a temple user', function(){
		expect(userPage.header.getText()).toBe('New User');
		userPage.email.sendKeys('test2@test.com');
		userPage.password.sendKeys('password');
		userPage.password2.sendKeys('password');
		userPage.peopleUser.click();
		userPage.save.click();
		
	});

	//it('Test select2 person', function(){
		// jQueryFunction('#s2id_person', 'select2', 'open');
		// jQueryFunction('#s2id_person', 'select2', 'search', 'ip');
		// jQueryFunction('.select2-results li:eq(1)', 'click');
    

		// var select2 = element(by.css('div#s2id_person'));
		// select2.getAttribute('class').then(function(clazz){
		// 	console.log('class'+clazz);
		// });
		// select2.click();
		// select2.sendKeys('ip');
		// select2.sendKeys(protractor.Key.ENTER);
		// var lis = element.all(by.css('li.select2-results-dept-0'));
		// lis.then(function(li) {
  //   		li[0].click();
		// });

		//userPage.person.sendKeys('ip'+protractor.Key.ENTER);
		
	//});

	it('Check that temple user is properly saved', function(){
		expect(userPage.header.getText()).toBe('Users');
		expect(userPage.firstUser.getText()).toBe('test2@test.com');
		userPage.edit(userPage.firstUser);
		expect(userPage.header.getText()).toBe('Edit User');
		expect(userPage.email.getAttribute('value')).toBe('test2@test.com');
		//expect(userPage.personSel.getText()).toBe('Ipsita Roy');
		//userPage.person.sendKeys('ra\n');
		//userPage.save.click();
		//expect(userPage.header.getText()).toBe('Users');
		//userPage.edit(userPage.firstUser);
		//expect(userPage.personSel.getText()).toBe('Radha Ramana Das');

	});

	it('Login with temple user', function(){	
		e2e.signout();
		e2e.login('test2@test.com');
	});

	it('Check authorization of temple user', function(){
		expect(e2e.getMenuItemEl('Temples')).toBeNull();
		expect(e2e.getMenuItemEl('Users')).toBeNull();
		expect(e2e.getMenuItemEl('People')).not.toBeNull();
		e2e.getMenuItemEl('People').then(function(menuEl){
			menuEl.click();	
			expect(e2e.getSubMenuEl('New Person')).toBeNull();
		});

		userPage.go('t/lenasia','users');
		expect(userPage.isDelete(userPage.firstUser)).toBe(false);
	});

	it('Login as temple admin', function(){
		e2e.signout();
		e2e.login('test3@test.com','password1');
	});

	it('Delete user as temple admin', function(){
		userPage.go('users');
		expect(userPage.firstUser.getText()).toBe('test2@test.com');
		userPage.delete(userPage.firstUser);
		userPage.yes();
		expect(userPage.firstUser.getText()).toBe('test3@test.com');
	});

	it('Login in as root', function(){
		e2e.signout();
		e2e.setBrowser();
		e2e.login('rory.gilfillan@gmail.com');
		userPage.loginTemple = null;
		userPage.go('users');
	});
	

	it('Delete a user as root', function(){
		expect(userPage.header.getText()).toBe('Users');
		expect(userPage.firstUser.getText()).toBe('test3@test.com');
		userPage.delete(userPage.firstUser);
		userPage.yes();
		expect(userPage.firstUser.getText()).not.toBe('test3@test.com');
	});
});