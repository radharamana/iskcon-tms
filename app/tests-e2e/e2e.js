'use strict';

module.exports.setBrowser = function(){
  browser.get('http://localhost:3000');
};

 module.exports.selectTemple =  function(){
  element(by.buttonText('Select Your Temple')).click();
	var temples = element.all(by.repeater('temple in temples'));
 	temples.first().click();
 	expect(browser.getLocationAbsUrl()).toContain('lenasia');
};

module.exports.login = function(user, pass){
  element(by.id('signin')).click();
  element(by.id('email')).sendKeys((user?user:'test@test.com'));
  element(by.id('password')).sendKeys((pass?pass:'password'));
  element(by.buttonText('Sign in')).click();
      
  browser.isElementPresent(by.binding('error')).then(function(isPresent){
  	   if(!isPresent)return;
    	element(by.binding('error')).getText().then(function(text){
  		console.log('login error:'+text);
    });	
  });	
	   	
  expect(element(by.binding('authentication.user.email')).isDisplayed()).toBe(true);
};

module.exports.signout = function(){
  	element(by.binding('authentication.user.email')).click();
  	element(by.id('signout')).click();
  	
  	expect(element(by.id('signin')).isDisplayed()).toBe(true);
  	expect(element(by.binding('authentication.user.email')).isDisplayed()).toBe(false);	
};

module.exports.getMenuItemEl = function(itemName){
  return element.all(by.repeater('item in menu.items')).filter(function(elem, index) {
    return elem.getText().then(function(text) {
      //console.log('text:'+text+' itemName:'+itemName+' '+text === itemName);
      return text === itemName;
    });
  }).then(function(els){
    if(els[0]){
      return els[0];  
    }else{
      return null;
    }
  }); 
};

module.exports.getSubMenuEl = function(itemName){
  return element.all(by.repeater('subitem in item.items')).filter(function(elem, index) {
      return elem.getText().then(function(text) {
        return text === itemName;
      });
  }).then(function(els){
    if(els[0]){
      return els[0];  
    }else{
      return null;
    }
  });
};

