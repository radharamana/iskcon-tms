'use strict';

var e2e = require('./e2e.js');

function changePassword(oldP, newP){
	element(by.binding('authentication.user.email')).click();
		element(by.id('setpassword')).click();
		element(by.id('currentPassword')).sendKeys((oldP?oldP:'password'));
		element(by.id('newPassword')).sendKeys((newP?newP:'password1'));
		element(by.id('verifyPassword')).sendKeys((newP?newP:'password1'));
		element(by.buttonText('Save Password')).click();
		
		expect(element(by.id('success')).isDisplayed()).toBe(true);
		expect(element(by.id('error')).isDisplayed()).toBe(false);
}

describe('Change Password on Root E2E tests', function(){
	it('init', function(){
		e2e.setBrowser();
		//e2e.selectTemple();
		e2e.login('rory.gilfillan@gmail.com', 'password');
	});

	it('change password', function(){
		changePassword();
	});

	it('signout', function(){
		e2e.signout();
	});

	it('reset password', function(){
		e2e.login('rory.gilfillan@gmail.com','password1');

	    changePassword('password1','password');
	});

	it('signout', function(){
		e2e.signout();
	});
});

describe('Change Password on Temple E2E tests', function(){
	it('init', function(){
		e2e.setBrowser();
		e2e.selectTemple();
		e2e.login();
	});

	it('change password', function(){
		changePassword();
	});

	it('signout', function(){
		e2e.signout();
	});

	it('reset password', function(){
		e2e.login('test@test.com','password1');

	    changePassword('password1','password');
	});

	it('signout', function(){
		e2e.signout();
	});
});
