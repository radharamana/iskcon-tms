'use strict';

var Page = require('astrolabe').Page;
var util = require('util');


module.exports = Page.create({
	url: {value:'http://localhost:3000/#!/'},
	brand: {get: function(){ return this.findElement(by.css('.navbar-brand')).getText(); }},
	showLogs: {value: function(){
			browser.manage().logs().get('browser').then(function(browserLog) {
				console.log('log: ' + util.inspect(browserLog));
			});
	}},
	//isError: {get: function(){  return  }},
	error: {get: function(){ 
		return browser.isElementPresent(this.by.id('error')).then(function(isError){
			//console.log('isError:'+isError);
			if(isError){
				return element(by.id('error')).getText().then(function(text){
					if(text)return text;
					else return null;
				});
			}else{
				return null;
			}
		});
	}},

	selectTemple: {value: function(templeName){
		element(by.buttonText('Select Your Temple')).click();
		return element.all(by.repeater('temple in temples')).filter(function(temple){
			return temple.getText().then(function(text){
				//console.log('text:'+text);
				return text === templeName;
			});
		}).then(function(temples){
			if(temples[0]){
				return temples[0].click();
			}else{
				throw new Error('No temple found');
			}
		});
	}},

	signin: {value: function(userName, password){
		element(by.id('signin')).click();
		element(by.id('email')).sendKeys((userName?userName:'test@test.com'));
		element(by.id('password')).sendKeys((password?password:'password'));
		element(by.buttonText('Sign in')).click();
		      
	  	browser.isElementPresent(by.binding('error')).then(function(isPresent){
	  		if(!isPresent)return;
		    element(by.binding('error')).getText().then(function(text){
		  		console.log('login error:'+text);
	    	});	
	  	});	
		   	
	  	expect(element(by.binding('authentication.user.email')).isDisplayed()).toBe(true);
	}},
	
	signout: {value: function(){
		element(by.binding('authentication.user.email')).click();
  		element(by.id('signout')).click();
  	
  		expect(element(by.id('signin')).isDisplayed()).toBe(true);
  		expect(element(by.binding('authentication.user.email')).isDisplayed()).toBe(false);	
	}},

	changePassword :{ value: function(oldP, newP ){
		element(by.binding('authentication.user.email')).click();
		element(by.id('setpassword')).click();
		element(by.id('currentPassword')).sendKeys((oldP?oldP:'password'));
		element(by.id('newPassword')).sendKeys((newP?newP:'password1'));
		element(by.id('verifyPassword')).sendKeys((newP?newP:'password1'));
		element(by.buttonText('Save Password')).click();
		
		expect(element(by.id('success')).isDisplayed()).toBe(true);
		expect(element(by.id('error')).isDisplayed()).toBe(false);
	}},

	menuItemEl: {value: function(itemName){
		return element.all(by.repeater('item in menu.items')).filter(function(elem, index) {
			return elem.getText().then(function(text) {
		      //console.log('text:'+text+' itemName:'+itemName+' '+text === itemName);
		      return text === itemName;
		    });
		}).then(function(els){
		    if(els[0]){
		      return els[0];  
		    }else{
		      return null;
		    }
		}); 
	}},
	subMenuItemEl:{value: function(itemName){
		return element.all(by.repeater('subitem in item.items')).filter(function(elem, index) {
			return elem.getText().then(function(text) {
		      //console.log('text:'+text+' itemName:'+itemName+' '+text === itemName);
		      return text === itemName;
		    });
		}).then(function(els){
		    if(els[0]){
		      return els[0];  
		    }else{
		      return null;
		    }
		}); 
	}},
	yes: {value: function(){ return this.findElement(this.by.buttonText('Yes')).click(); }},
	firstRow: {get: function(){ return element.all(by.repeater('row in renderedRows')).get(0); }},
	edit: {value: function(el){ return el.element(by.css('.glyphicon-edit')).click(); }},
	isDelete: {get: function(el){ return el.isElementPresnet(by.css('.glyphicon-trash')); }},
	delete: {value: function(el){ return el.element(by.css('.glyphicon-trash')).click(); }},
	save: {value: function(){ return this.findElement(this.by.id('save')).click(); }},
	save2: {get: function(){ return this.findElement(this.by.id('save')); }},
	saveModal: {value: function(){ return this.findElement(this.by.id('saveModal')).click(); }},
	create: {value: function(){ return this.findElement(this.by.id('create')).click(); }},
	cancel: {value: function(){ return this.findElement(this.by.buttonText('Cancel')).click(); }},
	clear: {value: function(){ return browser.driver.manage().deleteAllCookies(); }},
	header: {get: function(){return this.findElement(this.by.css('.page-header')).getText(); }},

	row: {value: 
		function(displayName){ 
			return element.all(by.repeater('row in renderedRows')).filter(function(row){
				return row.getText().then(function(text){
					//console.log('text:'+text+' '+(text.indexOf(displayName) !== -1));
					return text.indexOf(displayName) !== -1;
				});
			}).then(function(rows){
				//console.log('rows.length'+rows.length);
				if(rows[0]){
					return rows[0];
				}else{
					return null;
				}
			});
		}
	}
	
});

// module.exports.selectTemple =  function(){
//   element(by.buttonText('Select Your Temple')).click();
// 	var temples = element.all(by.repeater('temple in temples'));
//  	temples.first().click();
//  	expect(browser.getLocationAbsUrl()).toContain('lenasia');
// };