'use strict';

var Page = require('astrolabe').Page;
var util = require('util');



module.exports = Page.create({
	url: {value:'http://localhost:3000/#!'},
	goCreate: {value: function(temple){ this.go((temple?'t/'+temple+'/temples/create':'temples/create')); }},
	goList: {value: function(temple){ this.go((temple?'t/'+temple+'/temples':'temples')); }},
	header: {get: function(){return this.findElement(this.by.css('.page-header')).getText(); }},
	name: {get: function(){return this.findElement(this.by.id('name')); }},
	addrLine1: {get: function(){return this.findElement(this.by.id('line1')); }},
	pathname: {get: function(){return this.findElement(this.by.id('pathname')); }},
	save: {value: function(){ return this.findElement(this.by.buttonText('Save')).click(); }},

	edit: {value: function(templeEl){ templeEl.element(by.css('.glyphicon-edit')).click(); }},
	delete: {value: function(templeEl){ templeEl.element(by.css('.glyphicon-trash')).click(); }},
	firstTemple: {get: function(){ return element.all(by.repeater('temple in temples')).get(0); }},
	yes: {value: function(){ return this.findElement(this.by.buttonText('Yes')).click(); }}

}); 