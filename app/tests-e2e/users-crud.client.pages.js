'use strict';

var Page = require('astrolabe').Page;
var util = require('util');



module.exports = Page.create({
	url: {value:'http://localhost:3000/#!'},
	header: {get: function(){return this.findElement(this.by.css('.page-header')); }},
	email: {get: function(){return this.findElement(this.by.model('crudUser.email')); }},
	password: {get: function(){ return this.findElement(this.by.model('crudUser.password')); }},
	password2: {get: function(){ return this.findElement(this.by.model('form.password2')); }},
	peopleUser: {get: function(){ return this.findElement(this.by.model('roles.peopleUser')); }},
	peopleAdmin: {get: function(){ return this.findElement(this.by.model('roles.peopleAdmin')); }},
	templeAdmin: {get: function(){ return this.findElement(this.by.model('roles.templeAdmin')); }},
	systemAdmin: {get: function(){ return this.findElement(this.by.model('roles.systemAdmin')); }},
	temple: {get: function(){ return this.findElement(this.by.model('crudUser.temple')); }},
	templeSel: {get: function(){ return element(by.model('crudUser.temple')).$('option:checked'); }},
	isTemple: {get: function(){ return browser.isElementPresent(this.by.model('crudUser.temple')); }},
	person: {get: function(){ return this.findElement(this.by.model('crudUser.person')); }},
	isPerson: {get: function(){ return browser.isElementPresent(this.by.model('crudUser.person')); }},
	personSel: {get: function(){ return element(by.model('selects.person.value')).$('option:checked'); }},
	save: {get: function(){ return this.findElement(this.by.buttonText('Save')); }},
	edit: {value: function(userEl){ userEl.element(by.css('.glyphicon-edit')).click(); }},
	delete: {value: function(userEl){ userEl.element(by.css('.glyphicon-trash')).click(); }},
	isDelete: {value: function(userEl){ return userEl.isElementPresent(by.css('.glyphicon-trash')); }},
	firstUser: {get: function(){ return element.all(by.repeater('user in users')).get(0); }},
	yes: {value: function(){ return this.findElement(this.by.buttonText('Yes')).click(); }}

});