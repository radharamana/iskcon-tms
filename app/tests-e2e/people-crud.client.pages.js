'use strict';

var Page = require('astrolabe').Page;
var util = require('util');
var gridTestUtils = require( './gridTestUtils.spec.js'),
	uiSelectUtils = require('./uiSelectTestUtils.spec.js');


module.exports = Page.create({
	url: {value:'http://localhost:3000/#!'},
	goCreate: {value: function(temple){ this.go((temple?'t/'+temple+'/people/create':'people/create')); }},
	goList: {value: function(temple){ this.go((temple?'t/'+temple+'/people':'people')); }},
	firstName: {get: function(){ return this.findElement(this.by.id('firstName')); }},
	lastName: {get: function(){ return this.findElement(this.by.id('lastName')); }},
	email: {get: function(){ return this.findElement(this.by.id('email')); }},
	mobileNumber: {get: function(){ return this.findElement(this.by.id('mobileNumber')); }},
	workNumber: {get: function(){ return this.findElement(this.by.id('workNumber')); }},
	homeNumber: {get: function(){ return this.findElement(this.by.id('homeNumber')); }},
	wa_number: {get: function(){ return element(by.id('workAddress.numberBuilding')); }},
	workAddress_sendKeys: { value: function(keys){ return uiSelectUtils.one.sendKeys('workAddress',keys); }},
	workAddress: {get: function(){ return uiSelectUtils.one.value('workAddress'); }},
	ha_number: {get: function(){ return element(by.id('homeAddress.numberBuilding')); }},
	homeAddress_sendKeys: { value: function(keys){	return uiSelectUtils.one.sendKeys('homeAddress',keys); }},
	homeAddress: {get: function(){ return uiSelectUtils.one.value('homeAddress'); }},
	pa_line1: {get: function(){ return element(by.id('postalAddress.line1')); }},
	pa_line2: {get: function(){ return this.findElement(this.by.id('postalAddress.line2')); }},
	pa_suburb: {get: function(){ return this.findElement(this.by.id('postalAddress.suburb')); }},
	pa_postcode: {get: function(){ return this.findElement(this.by.id('postalAddress.postcode')); }},
	sex_male: {get: function(){ return element.all(by.id('sex')).get(0); }},	
	sex_female: {get: function(){ return element.all(by.id('sex')).get(1); }},	
	race: {get: function(){ return this.findElement(this.by.id('race')); }},
	occupation: {get: function(){ return this.findElement(this.by.id('occupation')); }},
	birthday: {get: function(){ return this.findElement(this.by.id('birthday')); }},
	donorAcrdn: {value: function(){ return this.findElement(this.by.css('#donor > div.panel-heading > h4 > a')).click(); }},
	amount: {get: function(){ return this.findElement(this.by.id('amount')); }},
	period: {get: function(){ return this.findElement(this.by.id('period')); }},
	dayOfMonth: {get: function(){ return this.findElement(this.by.id('dayOfMonth')); }},
	annualIncrease: {get: function(){ return this.findElement(this.by.id('annualIncrease')); }},
	bank: {get: function(){ return this.findElement(this.by.id('bank')); }},
	accountName: {get: function(){ return this.findElement(this.by.id('accountName')); }},
	accountNumber: {get: function(){ return this.findElement(this.by.id('accountNumber')); }},
	btgDeliveryMethod: {get: function(){ return this.findElement(this.by.id('btgDeliveryMethod')); }},
	
	contactAcrdn: {value: function(){ return this.findElement(this.by.css('#contact > div.panel-heading > h4 > a')).click(); }},
	spiritualBackground: {get: function(){ return this.findElement(this.by.id('spiritualBackground')); }},
	relMan_sendKeys: { value: function(keys){ return uiSelectUtils.one.sendKeys('relationshipManager',keys); }},
	relMan: {get: function(){ return uiSelectUtils.one.value('relationshipManager'); }},
	lastContactDate: {get: function(){ return this.findElement(this.by.id('lastContactDate')); }},
	favourablity: {get: function(){ return this.findElement(this.by.id('favourablity')); }},
	
	devoteeAcrdn: {value: function(){ return this.findElement(this.by.css('#devotee > div.panel-heading > h4 > a')).click(); }},
	
	initiatedName: {get: function(){ return this.findElement(this.by.id('initiatedName')); }},
	initiatedLevel: {get: function(){ return this.findElement(this.by.id('initiatedLevel')); }},
	yearJoined: {get: function(){ return this.findElement(this.by.id('yearJoined')); }},
	attendance: {get: function(){ return this.findElement(this.by.id('attendance')); }},
	
	participation_sendKeys: {value: function(keys){ return uiSelectUtils.many.sendKeys('participation', keys); }},
	participation: {get: function(){ return uiSelectUtils.many.value('participation'); }},
	
	skills_sendKeys: {value: function(keys){  return uiSelectUtils.many.sendKeys('skills', keys); }},
	skills: {get: function(){ return uiSelectUtils.many.value('skills'); }},
	courses_sendKeys: {value: function(keys){ return uiSelectUtils.many.sendKeys('courses', keys); }},
	courses: {get: function(){  return uiSelectUtils.many.value('courses'); }},
	guru_sendKeys: {value: function(keys){ return uiSelectUtils.one.sendKeys('guru',keys); }},
	guru: {get: function(){ return uiSelectUtils.one.value('guru'); }},
	
	residentAcrdn: {value: function(){ return this.findElement(this.by.css('#resident > div.panel-heading > h4 > a')).click(); }},
	nextOfKin_sendKeys: {value: function(keys){ return uiSelectUtils.one.sendKeys('nextOfKin', keys); }},
	nextOfKin: {get: function(){ return uiSelectUtils.one.value('nextOfKin'); }},
	idNum: {get: function(){ return this.findElement(this.by.id('idNum')); }},
	dateMovedIn: {get: function(){ return this.findElement(this.by.id('dateMovedIn')); }},
	medicalConditions_sendKeys: { value: function(keys){ return uiSelectUtils.many.sendKeys('medicalConditions',keys); }},
	medicalConditions: {get:function(){ return uiSelectUtils.many.value('medicalConditions'); }},

	commentAcrdn: {value: function(){ return this.findElement(this.by.css('#comments > div.panel-heading > h4 > a')).click(); }},
	comment: {get: function(){ return this.findElement(this.by.id('comment')); }},
	createComment: {value: function(){ return this.findElement(this.by.css('.glyphicon-plus')).click(); }},
	filterPeople: {value: function(name, expect){ 
		gridTestUtils.enterFilterInColumn('peopleGrid', 0, name);
		//var rowCount = gridTestUtils.rowCount('peopleGrid');
		// browser.wait(function(){
		// 	var deferred = protractor.promise.defer();
		// 	console.log('waiting');
		// 	rowCount.then(function(){
		// 		console.log('waiting over');
		// 		deferred.fulfill(true);
		// 	});
		// 	return deferred.promise();		
		// });
		return gridTestUtils.rowCount('peopleGrid').then(function(count){
			if(count){
				//console.log('returning row');
				return gridTestUtils.dataCell('peopleGrid',0,5);	
			}else{
				return null;
			}
		});
		
		
		//gridTestUtils.expectRowCount('peopleGrid',1);
	}},
	commentText: {value: function(row){ return gridTestUtils.dataCell('commentGrid',(row?row:0),1).getText(); }},
	commentAction: {value: function(row){ return gridTestUtils.dataCell('commentGrid',(row?row:0),3); }}
	
	
});