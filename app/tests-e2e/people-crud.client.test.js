'use strict';
var fs = require('fs');

var homePage = require('./home.client.page.js'),
	peoplePage = require('./people-crud.client.pages.js'),
	util = require('util');
var gridTestUtils = require( './gridTestUtils.spec.js');


function writeScreenShot(data, filename) {
    var stream = fs.createWriteStream(filename);

    stream.write(new Buffer(data, 'base64'));
    stream.end();
}

describe('People CRUD E2E tests', function(){
	it('should login as temple user', function(){
		homePage.clear();
		homePage.go();
		homePage.selectTemple('Lenasia');
		homePage.signin();
	});

	it('should delete test person if already exists', function(){
		peoplePage.goList('lenasia');
		expect(homePage.header).toBe('People');
		peoplePage.filterPeople('Test Das').then(function(el){
			if(el){
				homePage.delete(el);
				homePage.yes();
			}	
		});
		
	});

	it('should create a new person', function(){
		peoplePage.goCreate('lenasia');
		//homePage.showLogs();
		expect(homePage.header).toBe('New Person');

		peoplePage.firstName.sendKeys('Testname');
		peoplePage.lastName.sendKeys('Testsurname');
		peoplePage.email.sendKeys('test1@test.com');
		peoplePage.mobileNumber.sendKeys('1111111111');
		peoplePage.workNumber.sendKeys('222-222-2222');
		peoplePage.homeNumber.sendKeys('333 333 3333');
		//peoplePage.homeAdress.sendKeys('Boeing Road East, Germiston, South Africa');
		peoplePage.ha_number.sendKeys('12 Manhatttan');
		peoplePage.homeAddress_sendKeys('Libra Road, Germiston, 1401, South Africa');

		peoplePage.wa_number.sendKeys('11');
		peoplePage.workAddress_sendKeys('Boeing Road East, Germiston, South Africa');
		// peoplePage.workAddress.then(function(workAddress){
		// 	workAddress.sendKeys('Boeing Road East, Germiston, South Africa');
		// 	workAddress.sendKeys(protractor.Key.ENTER);
		// });
		
		peoplePage.pa_line1.sendKeys('postal address line 1');
		peoplePage.pa_line2.sendKeys('postal address line 2');
		peoplePage.pa_suburb.sendKeys('postal address suburb');
		peoplePage.pa_postcode.sendKeys('pa111');
		peoplePage.sex_male.click();
		peoplePage.race.sendKeys('White');
		peoplePage.occupation.sendKeys('test occupation');
		peoplePage.birthday.sendKeys('1/1/2000');
	});
	
	it('test donor', function(){
		peoplePage.donorAcrdn();
		peoplePage.amount.sendKeys('1000000');
		peoplePage.period.sendKeys('Annually');
		peoplePage.dayOfMonth.sendKeys('30');
		peoplePage.annualIncrease.sendKeys('25');
		peoplePage.bank.sendKeys('Test bank');
		peoplePage.accountName.sendKeys('Test account name');
		peoplePage.accountNumber.sendKeys('1324564567');
		peoplePage.btgDeliveryMethod.sendKeys('Fetch');
	});
	
	it('test contact', function(){
		peoplePage.contactAcrdn();
		peoplePage.spiritualBackground.sendKeys('Impersonalist');
		peoplePage.relMan_sendKeys('Ipsita');
		peoplePage.lastContactDate.sendKeys('1/1/2013');
		peoplePage.favourablity.sendKeys('Visiting');
	});

	it('test devotee', function(){
		peoplePage.devoteeAcrdn();
		peoplePage.initiatedName.sendKeys('Test Das');
		peoplePage.initiatedLevel.sendKeys('Second');
		peoplePage.yearJoined.sendKeys('2000');
		peoplePage.attendance.sendKeys('Weekly');
		peoplePage.participation_sendKeys('Donor');
		peoplePage.skills_sendKeys('New Skill');
		peoplePage.guru_sendKeys('Bha');
	});
	
	it('test resident', function(){	
		peoplePage.residentAcrdn();
		peoplePage.nextOfKin_sendKeys('Radha');
		peoplePage.dateMovedIn.sendKeys('01/06/2012');
		peoplePage.idNum.sendKeys('T12345454');
		peoplePage.medicalConditions_sendKeys('Asthma');
	});
	
	it('test comment', function(){

		peoplePage.commentAcrdn();
		peoplePage.comment.sendKeys('Test Comment');
		
		homePage.save();

		expect(homePage.error).toBeNull();

	});

	it('should test that create is properly saved', function(){
		expect(homePage.header).toBe('People');
//		var testPerson;
		
		peoplePage.filterPeople('Test Das').then(function(el){
			homePage.edit(el);

			//homePage.showLogs();
		//homePage.row('Test Das').then(function(testPerson){
			//expect(testPerson).not.toBeNull();
			// testPerson.getText().then(function(text){
			// 	console.log('testPerson.getText()'+text);	
			// });
			
			//expect(testPerson.getText()).toContain('Test Das');

			//homePage.edit(testPerson);

			expect(homePage.header).toBe('Edit Person');

			expect(peoplePage.firstName.getAttribute('value')).toBe('Testname');
			expect(peoplePage.lastName.getAttribute('value')).toBe('Testsurname');
			expect(peoplePage.email.getAttribute('value')).toBe('test1@test.com');
			expect(peoplePage.mobileNumber.getAttribute('value')).toBe('1111111111');
			expect(peoplePage.workNumber.getAttribute('value')).toBe('222-222-2222');
			expect(peoplePage.homeNumber.getAttribute('value')).toBe('333 333 3333');
			expect(peoplePage.homeAddress).toBe('Libra Road, Germiston, 1401, South Africa');
			expect(peoplePage.ha_number.getAttribute('value')).toBe('12 Manhatttan');

			expect(peoplePage.workAddress).toBe('Boeing East Road, Germiston, South Africa');
			expect(peoplePage.wa_number.getAttribute('value')).toBe('11');
			expect(peoplePage.pa_postcode.getAttribute('value')).toBe('pa111');
			expect(peoplePage.pa_line1.getAttribute('value')).toBe('postal address line 1');
			expect(peoplePage.pa_line2.getAttribute('value')).toBe('postal address line 2');
			expect(peoplePage.pa_suburb.getAttribute('value')).toBe('postal address suburb');
			expect(peoplePage.pa_postcode.getAttribute('value')).toBe('pa111');
			expect(peoplePage.sex_male.getAttribute('value')).toBe('Male');
			expect(peoplePage.race.getAttribute('value')).toBe('White');
			expect(peoplePage.occupation.getAttribute('value')).toBe('test occupation');
			expect(peoplePage.birthday.getAttribute('value')).toBe('01/01/2000');
			
			peoplePage.donorAcrdn();
			expect(peoplePage.amount.getAttribute('value')).toBe('1000000');
			expect(peoplePage.period.getAttribute('value')).toBe('Annually');
			expect(peoplePage.dayOfMonth.getAttribute('value')).toBe('30');
			expect(peoplePage.annualIncrease.getAttribute('value')).toBe('25');
			expect(peoplePage.bank.getAttribute('value')).toBe('Test bank');
			expect(peoplePage.accountName.getAttribute('value')).toBe('Test account name');
			expect(peoplePage.accountNumber.getAttribute('value')).toBe('1324564567');
			expect(peoplePage.btgDeliveryMethod.getAttribute('value')).toBe('Fetch');
			
			peoplePage.contactAcrdn();
			expect(peoplePage.spiritualBackground.getAttribute('value')).toBe('Impersonalist');
			expect(peoplePage.relMan).toBe('Ipsita Roy');
			expect(peoplePage.lastContactDate.getAttribute('value')).toBe('01/01/2013');
			expect(peoplePage.favourablity.getAttribute('value')).toBe('Visiting');
			
			peoplePage.devoteeAcrdn();
			expect(peoplePage.initiatedName.getAttribute('value')).toBe('Test Das');
			expect(peoplePage.initiatedLevel.getAttribute('value')).toBe('Second');
			expect(peoplePage.yearJoined.getAttribute('value')).toBe('2000');
			expect(peoplePage.attendance.getAttribute('value')).toBe('Weekly');
			expect(peoplePage.participation).toBe('Donor');
			expect(peoplePage.skills).toBe('New Skill');
			expect(peoplePage.guru).toBe('Bhakti Swami');

			peoplePage.residentAcrdn();
			expect(peoplePage.nextOfKin).toBe('Radha Ramana Das');
			expect(peoplePage.dateMovedIn.getAttribute('value')).toBe('01/06/2012');
			expect(peoplePage.idNum.getAttribute('value')).toBe('T12345454');
			expect(peoplePage.medicalConditions).toBe('Asthma');
			
			peoplePage.commentAcrdn();
			expect(peoplePage.commentText(0)).toContain('Test Comment');
		
		//expect((testPerson = homePage.row('Test Das'))).not.toBeNull();
		// var testPerson = homePage.row('Test Das');
		// testPerson.then(function(x){
		// console.log('testPerson:'+util.inspect(testPerson));
		});
	});

	it('should edit a person', function(){


		peoplePage.firstName.clear();
		peoplePage.lastName.clear();
		peoplePage.firstName.sendKeys('Testname1');
		peoplePage.lastName.sendKeys('Testsurname1');
		peoplePage.email.clear();
		peoplePage.mobileNumber.clear();
		peoplePage.workNumber.clear();
		peoplePage.homeNumber.clear();
		peoplePage.email.sendKeys('test2@test.com');
		peoplePage.mobileNumber.sendKeys('1111111112');
		peoplePage.workNumber.sendKeys('222-222-2223');
		peoplePage.homeNumber.sendKeys('333 333 3334');

		//peoplePage.homeAddress.clear();
		//peoplePage.ha_number.clear();
		//peoplePage.workAddress.clear();
		peoplePage.wa_number.clear();
		//peoplePage.ha_line1.clear();
		//peoplePage.ha_line2.clear();
		//peoplePage.ha_suburb.clear();
		//peoplePage.ha_postcode.clear();
		//peoplePage.wa_line1.clear();
		//peoplePage.wa_line2.clear();
		//peoplePage.wa_suburb.clear();
		//peoplePage.wa_postcode.clear();
		peoplePage.pa_line1.clear();
		peoplePage.pa_line2.clear();
		peoplePage.pa_suburb.clear();
		peoplePage.pa_postcode.clear();
		peoplePage.occupation.clear();
		peoplePage.birthday.clear();

		// peoplePage.ha_line1.sendKeys('home address line 11');
		// peoplePage.ha_line2.sendKeys('home address line 21');
		// peoplePage.ha_suburb.sendKeys('home address suburb1');
		// peoplePage.ha_postcode.sendKeys('hm1111');
		// peoplePage.wa_line1.sendKeys('work address line 11');
		// peoplePage.wa_line2.sendKeys('work address line 22');
		// peoplePage.wa_suburb.sendKeys('work address suburb1');
		// peoplePage.wa_postcode.sendKeys('wa1111');
		peoplePage.pa_line1.sendKeys('postal address line 11');
		peoplePage.pa_line2.sendKeys('postal address line 21');
		peoplePage.pa_suburb.sendKeys('postal address suburb1');
		peoplePage.pa_postcode.sendKeys('pa1111');
		peoplePage.sex_female.click();
		peoplePage.race.sendKeys('Black');
		peoplePage.occupation.sendKeys('test occupation1');
		peoplePage.birthday.sendKeys('2/1/2000');

		peoplePage.amount.clear();
		peoplePage.dayOfMonth.clear();
		peoplePage.annualIncrease.clear();
		peoplePage.bank.clear();
		peoplePage.accountName.clear();
		peoplePage.accountNumber.clear();
		
		peoplePage.amount.sendKeys('1000001');
		peoplePage.period.sendKeys('Monthly');
		peoplePage.dayOfMonth.sendKeys('29');
		peoplePage.annualIncrease.sendKeys('24');
		peoplePage.bank.sendKeys('Test bank');
		peoplePage.accountName.sendKeys('Test account name1');
		peoplePage.accountNumber.sendKeys('1324564568');
		peoplePage.btgDeliveryMethod.sendKeys('Post');
		
		
		


		
		
		//peoplePage.donorAcrdn();
		
		//peoplePage.contactAcrdn();
		peoplePage.lastContactDate.clear();
		//peoplePage.devoteeAcrdn();
		peoplePage.initiatedName.clear();
		peoplePage.yearJoined.clear();
		//peoplePage.residentAcrdn();
		peoplePage.dateMovedIn.clear();
		peoplePage.idNum.clear();

		
		//peoplePage.donorAcrdn();
		
		//peoplePage.contactAcrdn();
		peoplePage.spiritualBackground.sendKeys('Hindu');
		peoplePage.lastContactDate.sendKeys('2/1/2013');
		peoplePage.favourablity.sendKeys('Seva');
		//peoplePage.devoteeAcrdn();
		peoplePage.initiatedName.sendKeys('Test Das1');
		peoplePage.initiatedLevel.sendKeys('First');
		peoplePage.yearJoined.sendKeys('2001');
		peoplePage.attendance.sendKeys('Daily');
		//peoplePage.residentAcrdn();
		peoplePage.idNum.sendKeys('T123454546');
		peoplePage.dateMovedIn.sendKeys('02/06/2012');
		
		homePage.edit(peoplePage.commentAction(0));
		peoplePage.comment.clear();
		peoplePage.comment.sendKeys('Test Comment1');
		homePage.saveModal();

		homePage.create();
		peoplePage.comment.sendKeys('Test comment2');
		homePage.saveModal();
		
		homePage.save();		
  //  	 	browser.manage().logs().get('browser').then(function(browserLog) {
  // 			console.log('log: ' + require('util').inspect(browserLog));
		// });
   	 	expect(homePage.error).toBeNull();

   	 	expect(homePage.header).toBe('People');
		

	});

	it('should check that edit values are properly saved', function(){
		expect(homePage.header).toBe('People');

		peoplePage.filterPeople('Test Das1').then(function(el){
			homePage.edit(el);
		//homePage.row('Test Das1').then(function(testPerson){
			//expect(testPerson.getText()).toContain('Test Das1');
			//homePage.edit(testPerson);
			expect(peoplePage.firstName.getAttribute('value')).toBe('Testname1');
			expect(peoplePage.lastName.getAttribute('value')).toBe('Testsurname1');
			expect(peoplePage.email.getAttribute('value')).toBe('test2@test.com');
			expect(peoplePage.mobileNumber.getAttribute('value')).toBe('1111111112');
			expect(peoplePage.workNumber.getAttribute('value')).toBe('222-222-2223');
			expect(peoplePage.homeNumber.getAttribute('value')).toBe('333 333 3334');
			// expect(peoplePage.ha_line1.getAttribute('value')).toBe('home address line 11');
			// expect(peoplePage.ha_line2.getAttribute('value')).toBe('home address line 21');
			// expect(peoplePage.ha_suburb.getAttribute('value')).toBe('home address suburb1');
			// expect(peoplePage.ha_postcode.getAttribute('value')).toBe('hm1111');
			// expect(peoplePage.wa_line1.getAttribute('value')).toBe('work address line 11');
			// expect(peoplePage.wa_line2.getAttribute('value')).toBe('work address line 22');
			// expect(peoplePage.wa_suburb.getAttribute('value')).toBe('work address suburb1');
			// expect(peoplePage.wa_postcode.getAttribute('value')).toBe('wa1111');
			expect(peoplePage.pa_line1.getAttribute('value')).toBe('postal address line 11');
			expect(peoplePage.pa_line2.getAttribute('value')).toBe('postal address line 21');
			expect(peoplePage.pa_suburb.getAttribute('value')).toBe('postal address suburb1');
			expect(peoplePage.pa_postcode.getAttribute('value')).toBe('pa1111');
			expect(peoplePage.sex_female.getAttribute('value')).toBe('Female');
			expect(peoplePage.race.getAttribute('value')).toBe('Black');
			expect(peoplePage.occupation.getAttribute('value')).toBe('test occupation1');
			expect(peoplePage.birthday.getAttribute('value')).toBe('02/01/2000');
			peoplePage.donorAcrdn();
			expect(peoplePage.amount.getAttribute('value')).toBe('1000001');
			expect(peoplePage.period.getAttribute('value')).toBe('Monthly');
			expect(peoplePage.dayOfMonth.getAttribute('value')).toBe('29');
			expect(peoplePage.annualIncrease.getAttribute('value')).toBe('24');
			expect(peoplePage.bank.getAttribute('value')).toBe('Test bank');
			expect(peoplePage.accountName.getAttribute('value')).toBe('Test account name1');
			expect(peoplePage.accountNumber.getAttribute('value')).toBe('1324564568');
			expect(peoplePage.btgDeliveryMethod.getAttribute('value')).toBe('Post');
			peoplePage.contactAcrdn();
			expect(peoplePage.spiritualBackground.getAttribute('value')).toBe('Hindu');
			expect(peoplePage.lastContactDate.getAttribute('value')).toBe('02/01/2013');
			expect(peoplePage.favourablity.getAttribute('value')).toBe('Seva');
			peoplePage.devoteeAcrdn();
			expect(peoplePage.initiatedName.getAttribute('value')).toBe('Test Das1');
			expect(peoplePage.initiatedLevel.getAttribute('value')).toBe('First');
			expect(peoplePage.yearJoined.getAttribute('value')).toBe('2001');
			expect(peoplePage.attendance.getAttribute('value')).toBe('Daily');
			peoplePage.residentAcrdn();
			expect(peoplePage.dateMovedIn.getAttribute('value')).toBe('02/06/2012');
			expect(peoplePage.idNum.getAttribute('value')).toBe('T123454546');
			peoplePage.commentAcrdn();
			expect(peoplePage.commentText(0)).toBe('Test Comment1');
			expect(peoplePage.commentText(1)).toBe('Test comment2');
		});
	});

	it('should be able to delete a person', function(){		
		peoplePage.goList('lenasia');
		//var testPerson;
		peoplePage.filterPeople('Test Das').then(function(el){
			expect(el).not.toBeNull();
			homePage.delete(el);
			homePage.yes();
			peoplePage.filterPeople('Test Das').then(function(el){
				expect(el).toBeNull();
			});	
		});
		
		
		
		//homePage.row('Test Das1').then(function(testPerson){
		//	expect(testPerson.getText()).toContain('Test Das1');
		//	homePage.delete(testPerson);
		//	homePage.yes();
		//	expect(homePage.row('Test Das1')).toBeNull();
		//});
		
	});

	
});
	
