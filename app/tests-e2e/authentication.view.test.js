'use strict';

var e2e = require('./e2e.js');


describe('Authentication E2E Tests', function() {
	it('should change temple', function() {
	    e2e.setBrowser();
	    e2e.selectTemple();
	});

	it('should login', function() {
	    e2e.login();
	});

	it('should signout', function(){
		e2e.signout();
	});
});