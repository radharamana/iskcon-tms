'use strict';

module.exports = {
	one:{
		sendKeys:function(id, keys){
			element(by.css('#'+ id )).click();
			browser.wait(function(){
				return browser.isElementPresent(by.css('#'+id+' > input.form-control.ui-select-search'));
			});	
			return element(by.css('#'+id+' > input.form-control.ui-select-search'))
				.sendKeys(keys)
				.sendKeys(protractor.Key.ENTER);
		},
		value:function(id){
			return element(by.css('#'+id+' > div > span > span.ui-select-match-text.pull-left > span')).getText();
		}
	},
	many:{
		sendKeys:function(id, keys){
			return element(by.css('#'+id+' > div > input')).sendKeys(keys).sendKeys(protractor.Key.ENTER);
		},
		value:function(id){
			return element(by.css('#'+id+' > div > span > span > span > span:nth-child(2) > span')).getText();
		}
	}

};