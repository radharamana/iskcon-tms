'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Person = mongoose.model('Person'),
	util = require('util'),
	_ = require('lodash');



/**
 * Globals
 */
var user, person;

/**
 * Unit tests
 */
describe('Person Model Unit Tests:', function() {
	beforeEach(function(done) {
		user = new User({
			email: 'test@test.com',
			password: 'password'
		});

		user.save(function() { 
			person = new Person({
				name: { firstName:'Person', lastName:'Last'} ,
				email: 'person@earth.com',

				user: user
			});

			done();
		});
	});

	describe('Method Save', function() {
		it('should be able to save without problems', function(done) {
			return person.save(function(err) {
				should.not.exist(err);
				done();
			});
		});

		it('should be able to show an error when try to save without name', function(done) { 
			person.name = '';

			return person.save(function(err) {
				should.exist(err);
				done();
			});
		});

		it('should set the correct person type when trying to save non-dev', function(done) {
			
			return person.save(function(err){
				person.personType.should.containEql('Non-devotee');
				done();
			});
		});

		it('should set the correct person type when trying to save devotee', function(done) {
			person.devotee.yearJoined = 2000;

			return person.save(function(err){
				person.personType.should.containEql('Devotee');
				done();
			});
		});

		it('should set the correct displayName to when initiated', function(done) {
			person.devotee.initiatedName = 'Krishna Das';

			return person.save(function(err){
				person.displayName.should.containEql('Krishna Das');
				done();
			});
		});

		it('should set the correct displayName to when not initiated', function(done) {
			//person.devotee.initiatedName = 'Krishna Das';

			return person.save(function(err){
				person.displayName.should.containEql('Person Last');
				done();
			});
		});

		it('should set the correct initiated level number', function(done) {
			person.devotee.initiatedLevel.text = 'First';

			return person.save(function(err){
				person.devotee.initiatedLevel.num.should.be.equal(3);
				done();
			});
		});

		// it('should give error when date format is incorrect', function(done) {
		// 	person.birthday = '27/12/1979';

		// 	return person.save(function(err){
		// 		should.exist(err);
		// 		if(err)console.log('err:'+util.inspect(err));
		// 		done();
		// 	});
		// });
	});

	afterEach(function(done) { 
		Person.remove().exec();
		User.remove().exec();

		done();
	});
});