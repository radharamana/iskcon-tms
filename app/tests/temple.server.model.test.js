'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Temple = mongoose.model('Temple');

/**
 * Globals
 */
var user, temple;

/**
 * Unit tests
 */
describe('Temple Model Unit Tests:', function() {
	beforeEach(function(done) {
		user = new User({
			email: 'test@test.com',
			username: 'username',
			password: 'password'
		});

		user.save(function() { 
			temple = new Temple({
				name: 'Temple Name',
				pathname:'templePath',
				user: user
			});

			done();
		});
	});

	describe('Method Save', function() {
		it('should be able to save without problems', function(done) {
			return temple.save(function(err) {
				should.not.exist(err);
				done();
			});
		});

		it('should be able to show an error when try to save without name', function(done) { 
			temple.name = '';

			return temple.save(function(err) {
				should.exist(err);
				done();
			});
		});
	});

	afterEach(function(done) { 
		Temple.remove().exec();
		User.remove().exec();

		done();
	});
});