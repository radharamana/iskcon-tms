'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
	mongoose = require('mongoose'),
	async = require('async'),
	User = mongoose.model('User'),
	Person = mongoose.model('Person'),
	Temple = mongoose.model('Temple'),
	_ = require('lodash');
var util = require('util');

var request = require('supertest');
var server = request.agent('http://localhost:3001');
var people = require('../../app/controllers/people');
/**
 * Globals
 */
var user, person1, person2, person3, templex;

var loginUser = function() {
    return function(done) {
        function onResponse(err, res) {
           if (err) return done(err);
           //console.log('res:'+util.inspect(res));
           if(res.statusCode === 400) return done(res.body.message);
           should.exist(res.headers['set-cookie']);
           return done();
        }

        server
            .post('/t/templex/auth/signin')
            .send({ email: 'test@test.com', password: 'password' })
            //.expect(302)
            //.expect('Location', '/')
            .end(onResponse);
    };
};
/**
 * Unit tests
 */

function getFuncsSaveDocs(docs){
	var functions = [];

	var docCall = function(doc){
		return function(callback){
			doc.save(callback);
	    };
	};

	for (var i=0; i < docs.length; i++) {
	    functions.push(docCall(docs[i]));
    }
    return functions;
}

describe('Person Contoller Unit Tests:', function() {
	before(function(done) {
		user = new User({
			email: 'test@test.com',
			password: 'password',
			provider:'local',
			roles:['peopleAdmin']
		});

		templex = new Temple({
				name: 'Temple X',
				pathname:'templex'
		});

		person1 = new Person({
				name: { firstName:'Person', lastName:'Last'} ,
				devotee: {
					initiatedName: 'Bhakti Swami',
					initiatedLevel: { num:6, text:'Sannyasi / Guru'}
				},
				email: 'person@earth.com'
		});	

		person2 = new Person({
			name: { firstName:'Person', lastName:'Last'} ,
			email: 'person2@earth.com'
		});

		person3 = new Person({
			name: { firstName:'Person3', lastName:'Last'},
			email: 'person3@earth.com'
		});
		
		templex.save(function(){
			person1.temple = templex._id;
			person2.temple = templex._id;
			user.temple = templex._id;

			async.parallel(getFuncsSaveDocs([user, person2, person1]), function(err, results) {
    			if(err)console.log(err);
    			done();

    			
    			
			});
		});

		
	});

	describe('Testing Session', function() {
		it('login', loginUser());

		it('people.findAll should return correct list for initiatedLevel sorted by displayName',function(done){
			server
				.get('/t/templex/people?initiatedLevel=3') 
				.end(function(err, res){
					var body = res.body;
					people = res.body;
					people.should.have.length(1);
				});

			//console.log('/t/templex/people?initiatedLevel=0');
			server
				.get('/t/templex/people?initiatedLevel=0') 
				.end(function(err, res){
					var body = res.body;
					people = res.body;
					people[0].displayName.should.be.equal('Bhakti Swami');
					people.should.have.length(2);
					done();
				});
		});

		// it('should validate the birthday date correctly',function(done){
		// 	person3.birthday = 0;
 			
 	// 		console.log('person3:'+person3);
		//     server
		//       .post('/t/templex/people')
		//       .send(person3)
		//       .end(function(err, res){
		//       	res.body.should.containEql(person3);
		//       	done();
		//       });
			
		// });


	});

	after(function(done) { 
		Person.remove().exec();
		User.remove().exec();
		Temple.remove().exec();

		done();
	});
});