'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users');
	var people = require('../../app/controllers/people');
	var core = require('../../app/controllers/core');

	// People Routes
	app.route('/t/:temple/people')
		.get(people.list)
		.post(users.requiresLogin, users.hasAuthorization(['peopleAdmin', 'templeAdmin']), people.create);

	app.route('/t/:temple/people/:personId')
		.get(people.read)
		.put(users.requiresLogin, users.hasAuthorization(['peopleAdmin', 'templeAdmin']), people.update)
		.delete(users.requiresLogin, users.hasAuthorization(['peopleAdmin','templeAdmin']), people.delete);

	// Finish by binding the Person middleware
	app.param('personId', people.personByID);
	//app.param('temple', temples.templeByPath)
};