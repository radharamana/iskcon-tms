'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users');
	var temples = require('../../app/controllers/temples');

	// Temples Routes
	app.route('/t')
		.get(temples.list);
		

	app.route('/t/:temple')
		.get(temples.read);
		

	app.route('/temples/:templeId')
		.get(temples.read)
		.put(users.requiresLogin, users.hasAuthorization(['systemAdmin']), temples.update)
		.delete(users.requiresLogin, users.hasAuthorization(['systemAdmin']), temples.delete);

	app.route('/temples')
		.get(temples.list)
		.post(users.requiresLogin, temples.create);		


	// Finish by binding the Temple middleware
	app.param('temple', temples.templeByPath);
	app.param('templeId', temples.templeById);
};