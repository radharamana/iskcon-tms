'use strict';

/**
 * Module dependencies.
 */
var passport = require('passport');

module.exports = function(app) {
	// User Routes
	var users = require('../../app/controllers/users');
	var temples = require('../../app/controllers/temples');

	app.route('/user/me').get(users.me);
	app.route('/user/password').post(users.requiresLogin, users.changePassword);

	//app.route('/users/accounts').delete(users.removeOAuthProvider);


	// Setting up the users api
	app.route('/auth/signup').post(users.signup); //needed for initial db setup
	app.route('/auth/signin').post(users.signin);
	app.route('/auth/signout').get(users.signout);

	// // Setting the facebook oauth routes
	// app.route('/auth/facebook').get(passport.authenticate('facebook', {
	// 	scope: ['email']
	// }));
	// app.route('/auth/facebook/callback').get(users.oauthCallback('facebook'));

	// // Setting the twitter oauth routes
	// app.route('/auth/twitter').get(passport.authenticate('twitter'));
	// app.route('/auth/twitter/callback').get(users.oauthCallback('twitter'));

	// // Setting the google oauth routes
	// app.route('/auth/google').get(passport.authenticate('google', {
	// 	scope: [
	// 		'https://www.googleapis.com/auth/userinfo.profile',
	// 		'https://www.googleapis.com/auth/userinfo.email'
	// 	]
	// }));
	// app.route('/auth/google/callback').get(users.oauthCallback('google'));

	// // Setting the linkedin oauth routes
	// app.route('/auth/linkedin').get(passport.authenticate('linkedin'));
	// app.route('/auth/linkedin/callback').get(users.oauthCallback('linkedin'));

	//user crud
	app.route('/users')
		.get(users.requiresLogin, users.list)
		.post(users.requiresLogin, users.hasAuthorization(['systemAdmin']), users.create);

	app.route('/users/:userId')
		.get(users.requiresLogin, users.read)
		.put(users.requiresLogin,  users.hasAuthorization(['systemAdmin']), users.update)
		.delete(users.requiresLogin, users.hasAuthorization(['systemAdmin']), users.delete);

	app.route('/t/:temple/users')
		.get(users.requiresLogin, users.listByTemple)
		.post(users.requiresLogin, users.hasAuthorization(['systemAdmin','templeAdmin']), users.create);

	app.route('/t/:temple/users/:userId')
		.get(users.requiresLogin, users.read)
		.put(users.requiresLogin,  users.hasAuthorization(['systemAdmin','templeAdmin']), users.update)
		.delete(users.requiresLogin, users.hasAuthorization(['systemAdmin','templeAdmin']), users.delete);

	
	//temple specific users
	app.route('/t/:temple/user/me').get(users.me);
	//app.route('/t/:temple/users').put(users.requiresLogin, users.update);
	app.route('/t/:temple/user/password').post(users.requiresLogin, users.changePassword);
	//app.route('/t/:temple/users/accounts').delete(users.removeOAuthProvider);

	// Setting up the users api
	app.route('/t/:temple/auth/signup').post(users.signup);
	app.route('/t/:temple/auth/signin').post(users.signin);
	app.route('/t/:temple/auth/signout').get(users.signout);

	
	// Finish by binding the user middleware
	app.param('userId', users.userByID);
	//app.param('temple', temples.setTemple);
};