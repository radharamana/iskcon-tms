'use strict';

module.exports = function(app) {
	// Root routing
	var core = require('../../app/controllers/core');
	app.use(core.setNoCache);
	app.route('/').get(core.index);
	
};