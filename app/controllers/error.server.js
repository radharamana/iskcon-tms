'use strict';
/**
 * Get the error message from error object
 */
exports.process = function(err) {
	var messages = [];
	
	if (err.code) {
		switch (err.code) {
			case 11000:
			case 11001:
				messages.push('Duplicate already exists');
				break;
			default:
				messages.push('Something went wrong');
		}
	} else if(err.errors){
		
		for (var errName in err.errors) {
			if (err.errors[errName].message){
				messages.push(err.errors[errName].message);	
			} 
		}

	}else if(err.message){
		messages.push(err.message);
	}else{
		messages.push(err);
	}

	return JSON.stringify(messages.reverse());
};