'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Person = mongoose.model('Person'),
	_ = require('lodash'),
	error = require('./error.server.js');

/**
 * Get the error message from error object
 */
// var getErrorMessage = function(err) {
// 	var messages = [];
	
// 	if (err.code) {
// 		switch (err.code) {
// 			case 11000:
// 			case 11001:
// 				messages.push('Person already exists');
// 				break;
// 			default:
// 				messages.push('Something went wrong');
// 		}
// 	} else if(err.errors){
		
// 		for (var errName in err.errors) {
// 			if (err.errors[errName].message){
// 				messages.push(err.errors[errName].message);	
// 			} 
// 		}

// 	}else{
// 		messages.push(err.message);
// 	}

// 	return messages.reverse();
// };

/**
 * Create a Person
 */
exports.create = function(req, res) {
	var person = new Person(req.body);
	//person.user = req.user;
	person.temple = req.temple._id;

	person.save(function(err) {
		if (err) {
			return res.send(400, error.process(err));
		} else {
			res.jsonp(person);
		}
	});
};

/**
 * Show the current Person
 */
exports.read = function(req, res) {
	res.jsonp(req.person);
};

/**
 * Update a Person
 */
exports.update = function(req, res) {
	var person = req.person ;

	person = _.extend(person , req.body);

	person.save(function(err) {
		if (err) {
			return res.send(400, error.process(err));
		} else {
			res.jsonp(person);
		}
	});
};

/**
 * Delete an Person
 */
exports.delete = function(req, res) {
	var person = req.person ;

	person.remove(function(err) {
		if (err) {
			return res.send(400, error.process(err));
		} else {
			res.jsonp(person);
		}
	});
};

/**
 * List of People
 */
exports.list = function(req, res) { 
		var personCallback = function(err, people){
			if (err) {
				return res.send(400, error.process(err));
			} else {
				//console.log('here');
				res.jsonp(people);
			}
		};

		if(req.param('initiatedLevel')){
			Person.find({'temple':req.temple._id}).select('_id displayName')
				.where('devotee.initiatedLevel.num').gte(req.param('initiatedLevel'))
				.sort('displayName').setOptions({lean: true}).exec(personCallback);
		}else{
			Person.find({ 'temple':req.temple._id}).exec(personCallback);
		}
		
};

/**
 * Person middleware
 */
exports.personByID = function(req, res, next, id) { 
		Person.findById(id)
			.populate([
				{path: 'resident.nextOfKin',  select: '_id displayName'},
				{path: 'devotee.guru', select: '_id displayName'},
				{path: 'contact.relationshipManager', select: '_id displayName'},
				{path: 'comments.person', select: '_id displayName'},

			]).exec(function(err, person) {
				if (err) return next(err);
				if (! person) return next(new Error('Failed to load Person ' + id));
				req.person = person ;
				next();
			});
};




/**
 * Person authorization middleware
 */
// exports.hasAuthorization = function(roles) {
// 	return function(req, res, next) {
// 			if (_.intersection(req.user.roles, roles).length) {
// 					return next();
// 				} else {
// 					return res.send(403, error.process('User not authorized'));
// 				}
// 			};
		
// };


/**
 * User authorizations routing middleware
 */
// exports.hasAuthorization = function(roles) {
// 	var _this = this;

// 	return function(req, res, next) {
// 		_this.requiresLogin(req, res, function() {
// 			if (_.intersection(req.user.roles, roles).length) {
// 				return next();
// 			} else {
// 				return res.send(403, ['User is not authorized']);
// 			}
// 		});
// 	};
// };