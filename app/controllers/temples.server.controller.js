'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Temple = mongoose.model('Temple'),
	_ = require('lodash'),
	error = require('./error.server.js');

/**
 * Get the error message from error object
 */
// var getErrorMessage = function(err) {
// 	var message = '';

// 	if (err.code) {
// 		switch (err.code) {
// 			case 11000:
// 			case 11001:
// 				message = 'Temple already exists';
// 				break;
// 			default:
// 				message = 'Something went wrong';
// 		}
// 	} else if(err.errors){
		
// 		for (var errName in err.errors) {
// 			if (err.errors[errName].message) message = err.errors[errName].message;
// 		}
// 	}else{
// 		return err.message;
// 	}

// 	return message;
// };

/**
 * Create a Temple
 */
exports.create = function(req, res) {
	var temple = new Temple(req.body);
	temple.user = req.user;

	temple.save(function(err) {
		if (err) {
			return res.send(400, error.process(err));
		} else {
			res.jsonp(temple);
		}
	});
};

/**
 * Show the current Temple
 */
exports.read = function(req, res) {
	res.jsonp(req.temple);
};

/**
 * Update a Temple
 */
exports.update = function(req, res) {
	var temple = req.temple ;

	temple = _.extend(temple , req.body);

	temple.save(function(err) {
		if (err) {
			return res.send(400, error.process(err));
		} else {
			res.jsonp(temple);
		}
	});
};

/**
 * Delete an Temple
 */
exports.delete = function(req, res) {
	var temple = req.temple ;

	temple.remove(function(err) {
		if (err) {
			return res.send(400,error.process(err));
		} else {
			res.jsonp(temple);
		}
	});
};

/**
 * List of Temples
 */
exports.list = function(req, res) { Temple.find().sort('-created').exec(function(err, temples) {
		if (err) {
			return res.send(400,error.process(err));
		} else {
			res.jsonp(temples);
		}
	});
};

/**
 * Temple middleware
 */
exports.templeById = function(req, res, next, id) { Temple.findById(id).exec(function(err, temple) {
		if (err) return next(err);
		if (! temple) return next(new Error('Failed to load Temple ' + id));
		req.temple = temple ;
		next();
	});
};

exports.templeByPath = function(req, res, next, path) { Temple.findOne({'pathname':path}).exec(function(err, temple) {
		if (err) return next(err);
		if (! temple) return next(new Error('Failed to load Temple ' + path));
		req.temple = temple ;
		next();
	});
};

/**
 * Temple authorization middleware
 */
// exports.hasAuthorization = function(req, res, next) {
// 	if (req.temple.user.id !== req.user.id) {
// 		return res.send(403, error.process('User is not authorized'));
// 	}
// 	next();
// };

/**
 * Temple middleware
 */
exports.setTemple = function(req, res, next, temple) { 
	req.temple = temple;
	next();
};
