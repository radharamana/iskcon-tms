'use strict';

/**
 * Module dependencies.
 */
exports.index = function(req, res) {
			
			res.render('index', {
				user: req.user || null
			});
		
	};

exports.setNoCache = function(req, res, next){
	res.set({
				'Cache-Control': 'no-cache, no-store, must-revalidate',
				'Pragma': 'no-cache',
				'Expires': '0'
			});
	next();
};


/**
 * User authorizations routing middleware
 */
// exports.hasAuthorization = function(roles) {
// 	var _this = this;

// 	return function(req, res, next) {
// 		_this.requiresLogin(req, res, function() {
// 			if (_.intersection(req.user.roles, roles).length) {
// 				return next();
// 			} else {
// 				return res.send(403, ['User is not authorized']);
// 			}
// 		});
// 	};
// };
