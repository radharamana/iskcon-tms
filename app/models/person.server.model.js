'use strict';


/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	_ = require('lodash');

var isPopulated = function(object, exceptionField){
	var keys = Object.keys(object);
	for(var i=0; i < keys.length; i++){
		var key = keys[i];
		if(key === exceptionField)continue;
		if( Object.prototype.toString.call( object[key] ) === '[object Array]'){
			if(object[key].length > 0){
				return true;
			}	
		}else{
			
			if(
				key !== 'toObject' &&  
				typeof object[key] !== 'undefined'){
				return true;
			}
		}
	}

	return false;
};

var initLevelEnum = ['Non-devotee','New', '16 Rounds', 'First', 'Second','Sannyasi / Guru'];

// var valiDate = function(newDate){
// 	//^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$
// 	console.log('newDate:'+newDate);
// }

/**
 * Person Schema
 */
var PersonSchema = new Schema({
	name: { 
		firstName : { 
			type: String,
			trim: true,
			required: 'Please enter the person\'s first name'
		},
		lastName : {
			type: String,
			trim: true,
			required: 'Please enter the person\'s last name'
		} 
	},
	displayName:{
		type:String,
		index:true		
	},
	birthday:{
		type: Date
	} ,
	email:{ 
			type: String, 
			match: [/.+\@.+\..+/, 'Please fill a valid email address'],
			required: 'Please enter the persons email'
		},
	workNumber: String,
	homeNumber: String,
	mobileNumber: String,
	homeAddress: { numberBuilding: String, formatted_address: String, lat:Number, lng: Number},
	//homeAddress: { line1: String, line2: String, suburb: String, postcode:String, lat:Number, lon: Number},
	workAddress: { numberBuilding: String, formatted_address: String, lat:Number, lng: Number},
	//postalSame
	postalAddress: {line1: String, line2:String, suburb: String, postcode:String},
	sex: {type: String, enum: ['Male','Female'], default: 'Male'},
	occupation: {
		type: String
	},
	race: {
		type: String,
		enum: ['Indian', 'White', 'Black', 'Coloured', 'Oriental', 'Other' ]
	},
	personType: [{
		type:String,
		enum: ['Non-devotee','Contact','Donor','Devotee','Resident'],

	}],
	devotee: {
		initiatedName: String,
		initiatedLevel: {
			num:{
				type:Number,
				index:true
			},
			text:{
				type: String,
				enum: initLevelEnum,
				default: 'Non-devotee'		
			}
		},
		yearJoined: {
			type:Number,
			min:1960,
			max:2050
		},
		attendance: {
			type: String,
			enum: ['Daily','BiTriWeekly','Weekly','Monthly','Events']
		},
		participation: [{
			type: String,
			enum: ['Attends service', 'Diety Seva', 'Misc Seva', 'Donor', 'Outreach','Management']
		}],
		skills: [{ type: String }],
		courses: [{
			type: String
		}],
		guru: { 
			type: Schema.ObjectId,
			ref: 'Person'
		}
	},
	donor: {
		amount: {
			type: Number
		},
		period: {
			type: String,
			enum: ['Monthly', 'Irregular', 'Annually']
			
		},
		dayOfMonth: {
			type: Number,
			min: 1,
			max: 31
		},
		annualIncrease: {
			type: Number,
			
			min: 0,
			max: 25
		},
		bank: String,
		accountName: String,
		accountNumber: String,
		btgDeliveryMethod:{
			type: String,
			enum: ['No BTG','Post', 'Fetch']
			
		}
	},
	contact: {
		spiritualBackground: {
			type: String,
			enum: ['Hindu', 'Muslim', 'Christian', 'Athiest', 'Impersonalist', 'New Age', 'Jew', 'Other' ]
		},
		relationshipManager: {
			type: Schema.ObjectId,
			ref: 'Person'
		},
		lastContactDate: Date,
		favourablity: {
			type: String,
			enum: ['Offensive','Fryed', 'Neutral', 'Favourable', 'Reading or Chanting', 'Visiting', 'Seva']
		}
	},
	resident: {
		nextOfKin: {
			type: Schema.ObjectId,
			ref: 'Person'
		},
		idOrPass: String,
		dateMovedIn: Date,
		medicalConditions: [{ type: String}]
	},
	comments: [{
		date: Date,
		person: {
			type: Schema.ObjectId,
			ref: 'Person'
		},
		comment: String
	}],

	created: {
		type: Date,
		default: Date.now
	},
	
	temple: {
		type: String,
		ref: 'Temple',
		index:true
	}
});
PersonSchema.index({email:1, temple:1 },{unique:true});
/**
 * Hook a pre save method to hash the password
 */
PersonSchema.pre('save', function(next) {
	this.setDisplayName();
	this.setPersonType();	
	this.setInitLevelNum();
	
	next();
});

PersonSchema.methods.setDisplayName = function(){
	if(this.devotee.initiatedName){
		this.displayName = this.devotee.initiatedName;
	}else{
		this.displayName = this.name.firstName + ' '+ this.name.lastName;
	}
};

PersonSchema.methods.setPersonType = function(){
	var nonDev = true;
	this.personType = [];
	if(isPopulated(this.devotee,'initiatedLevel')){
		this.personType.push('Devotee');
		nonDev = false;
	}
	if(isPopulated(this.donor)){
		this.personType.push('Donor');
		nonDev = false;
	}
	if(isPopulated(this.contact)){
		this.personType.push('Contact');
	}
	if(isPopulated(this.resident)){
		this.personType.push('Resident');
		nonDev = false;
	}
	if(nonDev){
		this.personType.push('Non-devotee');
	}
};

PersonSchema.methods.setInitLevelNum = function(){
	this.devotee.initiatedLevel.num = initLevelEnum.indexOf(this.devotee.initiatedLevel.text);
};

// UserSchema.methods.hashPassword = function(password) {
// 	if (this.salt && password) {
// 		return crypto.pbkdf2Sync(password, this.salt, 10000, 64).toString('base64');
// 	} else {
// 		return password;
// 	}
// };

mongoose.model('Person', PersonSchema);






