'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Temple Schema
 */
var TempleSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Temple name',
		trim: true
	},
	pathname: {
		type:String,
		unique: true,
		required: 'Pathname is required',
		match: [/[a-z0-9]+/,'Pathname must be only alphanumeric lowercase and no spaces']	
	},
	address: {line1: String, line2: String, line3: String, postcode:String, lat:Number, lon: Number},
	created: {
		type: Date,
		default: Date.now
	},
	picture: { mime: String, bin:Buffer },
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
	
});

mongoose.model('Temple', TempleSchema);